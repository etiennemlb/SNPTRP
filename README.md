# Crappy SNPTRP
A crappy library that aims to recreate a custom token ring implementation.

## To compile you might need:
cmake

gcc > 4 or clang

Its currently supported only on linux (mostly because of the socket lib).

## Compile:

$ rm -rf build
$ mkdir build && cd build

$ cmake .. # will default to release with debug info

$ make # or $ cmake --build . -- -j 16 # or $ make -j 16

## Generate the doc
To generate the documentation, you will need doxygen. It's a well known tool that parse the comment (in the code) and 
generate html (or other tpye) documentation.


$ doxygen makedoc.doxy

The generated doc start at "doc/html/index.html"
Inside, you will find a description of the api.

## Usage:

./app_demo PREV_NODE_LISTEN LOCAL_IP_TO_BIND NEXT_NODE_SEND PORT_LISTEN PORT_SEND [--origin]

port_listen and port_send must be different.

Once the ring is fully setup, you might want to use the following command.
Type:
c IP // To connect to the distant IP
d IP // To deconnect from IP
s IP msg // To send a message to IP
r IP // To recv (read the buffer of incoming segment from IP)

ex:
c 127.0.0.5
s 127.0.0.3 A pretty message
r 127.0.0.5
d 127.0.0.5