/**
 * @file debug.h
 * @author Etienne Malaboeuf
 * @brief 
 * @version 0.1
 * 
    Simple Non Pragmatic Token Ring Protocol
    Copyright (C) 2020  Etienne Malaboeuf

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

 * 
 */

#ifndef SIMPLE_LEXER_CONFIG_H
#define SIMPLE_LEXER_CONFIG_H

#ifdef NDEBUG
static inline void dbprintf(const char* fmt, ...)
{
    (void)fmt;
}
#else
/**
 * @brief Printf with a condition for debug
 * 
 * @param fmt format string
 * @param ... args
 */
void dbprintf(const char* fmt, ...);
#endif  // ! NDEBUG

#endif
