/**
 * @file socket_udp.c
 * @author Etienne Malaboeuf
 * @brief 
 * @version 0.1
 * 
    Simple Non Pragmatic Token Ring Protocol
    Copyright (C) 2020  Etienne Malaboeuf

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

 * 
 */

#include "socket_udp.h"

#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

/* RESOLVERS */

socket_UDPResolver_t* socket_CreateUDPResolver(socket_Query_t query)
{
    return socket_CreateResolver(query,
                                 ENUM_SOCKET_TYPE_DATAGRAM,
                                 ENUM_SOCKET_RESOLVER_FLAGS_PASSIVE); /* if query.node_name is not set, fall back to
                                                                         localhost bindable addr resolving */
}

socket_UDPResolver_t* socket_UDPResolverNext(socket_UDPResolver_t* it)
{
    return socket_ResolverNext(it);
}

socket_Endpoint_t* socket_UDPResolverGetEndpoint(const socket_UDPResolver_t* it)
{
    return socket_ResolverGetEndpoint(it);
}

void socket_FreeUDPResolver(socket_UDPResolver_t* resolver)
{
    socket_FreeResolver(resolver);
}

/* UDPSOCKET */

socket_UDPSocket_t socket_CreateUDPSocket(const socket_UDPResolver_t* it, bool reuse_addr)
{
    if(it == NULL || it->it_ == NULL || it->first_ == NULL)
    {
        errno = EFAULT;
        return -1;
    }

    socket_UDPSocket_t sock;

    socket_ResolverEndpointInfo_t__* endp = it->it_;

    if((sock = socket(endp->ai_family, endp->ai_socktype, endp->ai_protocol)) < 0)
    {
        return -1;
    }

    if(reuse_addr)
    {
        int val = 1;
        if(setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &val, sizeof(val)) < 0)
        {
            socket_CloseUDPSocket(sock);
            return -1;
        }

        if(setsockopt(sock, SOL_SOCKET, SO_REUSEPORT, &val, sizeof(val)) < 0)
        {
            socket_CloseUDPSocket(sock);
            return -1;
        }
    }

    if(bind(sock, endp->ai_addr, endp->ai_addrlen) < 0)
    {
        socket_CloseUDPSocket(sock);
        return -1;
    }

    /* memcpy(&sock->local_endp, endp->ai_addr, endp->ai_addrlen); */

    return sock;
}

socket_UDPSocket_t socket_CreateUDPConnectedSocket(const socket_UDPResolver_t* it)
{
    if(it == NULL || it->it_ == NULL || it->first_ == NULL)
    {
        errno = EFAULT;
        return -1;
    }

    socket_UDPSocket_t sock;

    socket_ResolverEndpointInfo_t__* endp = it->it_;

    if((sock = socket(endp->ai_family, endp->ai_socktype, endp->ai_protocol)) < 0)
    {
        return -1;
    }

    if(socket_UDPConnectEndpoint(sock, socket_UDPResolverGetEndpoint(it)) < 0)
    {
        socket_CloseUDPSocket(sock);
        return -1;
    }

    /*  memset(&sock->local_endp, 0, sizeof(socket_Endpoint_t)); */

    return sock;
}

int socket_UDPGetDestEndpoint(socket_UDPSocket_t sock, socket_Endpoint_t* endp)
{
    return socket_GetDestEndpoint(sock, endp);
}

int socket_UDPConnect(socket_UDPSocket_t sock, const socket_UDPResolver_t* it)
{
    return socket_UDPConnectEndpoint(sock, socket_UDPResolverGetEndpoint(it));
}

int socket_UDPConnectEndpoint(socket_UDPSocket_t sock, const socket_Endpoint_t* endp)
{
    // deconnect
    if(endp == NULL)
    {
        struct sockaddr_in deconnect_special_addr;
        deconnect_special_addr.sin_family = ENUM_SOCKET_FAMILY_UNSPEC;

        if(connect(sock, (struct sockaddr*)&deconnect_special_addr, sizeof(struct sockaddr_in)) < 0)
        {
            return -1;
        }

        return 0;
    }

    if(connect(sock, (struct sockaddr*)endp, sizeof(socket_Endpoint_t)) < 0)
    {
        return -1;
    }

    return 0;
}

int socket_CloseUDPSocket(socket_UDPSocket_t sock)
{
    return close(sock);
}

int socket_UDPReceive(socket_UDPSocket_t sock, void* buf, size_t count, size_t* bytes_read, socket_SockIOFlags_e flags)
{
    if(bytes_read == NULL)
    {
        errno = EFAULT;
        return -1;
    }

    size_t  total = 0;
    ssize_t bytes_recved;

    while(total < count)
    {
        bytes_recved = recv(sock, ((uint8_t*)buf) + total, count - total, flags);
        /* For udp, zero lenght packet are an ok thing, no explicit EOF */

        if(bytes_recved < 0)
        {
            *bytes_read = total;
            return -1; /* localy detected error */
        }

        total += bytes_recved;
    }

    *bytes_read = total;
    return 0;
}

int socket_UDPSend(socket_UDPSocket_t   sock,
                   const void*          buf,
                   size_t               len,
                   size_t*              bytes_written,
                   socket_SockIOFlags_e flags)
{
    if(bytes_written == NULL)
    {
        errno = EFAULT;
        return -1;
    }

    size_t total = 0;

    while(total < len)
    {
        ssize_t bytes_sent = send(sock, ((uint8_t*)buf) + total, len - total, flags);
        if(bytes_sent < 0)
        {
            *bytes_written = total;
            return -1; /* localy detected error */
        }
        total += bytes_sent;
    }
    *bytes_written = total;
    return 0;
}

ssize_t socket_UDPReceiveSome(socket_UDPSocket_t sock, void* buf, size_t count, socket_SockIOFlags_e flags)
{
    return recv(sock, (uint8_t*)buf, count, flags);
}

ssize_t socket_UDPSendSome(socket_UDPSocket_t sock, const void* buf, size_t len, socket_SockIOFlags_e flags)
{
    return send(sock, (uint8_t*)buf, len, flags);
}

ssize_t socket_UDPReceiveSomeFrom(socket_UDPSocket_t   sock,
                                  void*                buf,
                                  size_t               count,
                                  socket_SockIOFlags_e flags,
                                  socket_Endpoint_t*   from)
{
    socklen_t addr_len;
    return recvfrom(sock, buf, count, flags, (struct sockaddr*)from, &addr_len);
}

ssize_t socket_UDPSendSomeTo(socket_UDPSocket_t       sock,
                             const void*              buf,
                             size_t                   len,
                             socket_SockIOFlags_e     flags,
                             const socket_Endpoint_t* to)
{
    return sendto(sock, buf, len, flags, (struct sockaddr*)to, sizeof(socket_Endpoint_t));
}
