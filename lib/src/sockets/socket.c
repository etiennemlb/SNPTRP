/**
 * @file socket.c
 * @author Etienne Malaboeuf
 * @brief 
 * @version 0.1
 * 
    Simple Non Pragmatic Token Ring Protocol
    Copyright (C) 2020  Etienne Malaboeuf

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

 * 
 */

// non mandatory
// #include <sys/types.h>
// #include <sys/socket.h>
// #include <netdb.h>

#include "socket.h"

#include <arpa/inet.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

socket_EndpointStr_t* socket_EndpointToStr(const socket_Endpoint_t* endp)
{
    if(endp == NULL)
    {
        errno = EFAULT;
        return NULL;
    }

    socket_EndpointStr_t* str;

    if((str = (socket_EndpointStr_t*)malloc(sizeof(socket_EndpointStr_t))) == NULL)
    {
        return NULL;
    }

    if((str->node_name = (char*)malloc(SOCKET_IPV6_ADDR_STR_LEN)) == NULL)
    {
        free(str);
        return NULL;
    }

    if((str->service = (char*)malloc(SOCKET_INET_SERVICELEN)) == NULL)
    {
        free(str->node_name);
        free(str);
        return NULL;
    }

    void*    addr_ptr;
    uint16_t port;

    switch(endp->ss_family)
    {
        case ENUM_SOCKET_FAMILY_IPV4:
            addr_ptr = &(((struct sockaddr_in*)endp)->sin_addr);
            port     = ((struct sockaddr_in*)endp)->sin_port;
            break;
        case ENUM_SOCKET_FAMILY_IPV6:
            addr_ptr = &(((struct sockaddr_in6*)endp)->sin6_addr);
            port     = ((struct sockaddr_in6*)endp)->sin6_port;
            break;

        default:
            free(str->node_name);
            free(str->service);
            free(str);
            return NULL;
    }

    if(inet_ntop(endp->ss_family, addr_ptr, str->node_name, SOCKET_IPV6_ADDR_STR_LEN) == NULL)
    {
        free(str->node_name);
        free(str->service);
        free(str);
        return NULL;
    }

    str->family = endp->ss_family;
    snprintf(str->service, SOCKET_INET_SERVICELEN, "%hu", ntohs(port));

    return str;
}

int socket_GetDestEndpoint(socket_SocketFD_t sock_fd, socket_Endpoint_t* endp)
{
    socklen_t len;
    return getpeername(sock_fd, (struct sockaddr*)endp, &len);
}

void socket_FreeEndpointStr(socket_EndpointStr_t* endp)
{
    if(endp == NULL)
    {
        return;
    }

    free(endp->node_name);
    free(endp->service);
    free(endp);
}

int socket_IPV4ToStr(uint32_t addr, char* out)
{
    struct in_addr addr_s = {addr};
    if(inet_ntop(ENUM_SOCKET_FAMILY_IPV4, &addr_s, out, SOCKET_IPV4_ADDR_STR_LEN) == NULL)
    {
        return -1;
    }
    return 0;
}

int socket_StrToIPV4(const char* addr, uint32_t* out)
{
    struct in_addr addr_s;

    if(inet_pton(ENUM_SOCKET_FAMILY_IPV4, addr, &addr_s) < 0)
    {
        return -1;
    }
    *out = addr_s.s_addr;
    return 0;
}

/* RESOLVERS */

socket_Resolver_t* socket_CreateResolver(socket_Query_t query, socket_SockType_e type, socket_SockFlags_e flags)
{
    socket_Resolver_t* res;

    socket_ResolverEndpointInfo_t__ hints;

    if((res = (socket_Resolver_t*)malloc(sizeof(socket_Resolver_t))) == NULL)
    {
        return NULL;
    }

    memset(&hints, 0, sizeof(socket_ResolverEndpointInfo_t__));

    hints.ai_family   = query.family;
    hints.ai_socktype = type;
    hints.ai_flags    = flags;
    hints.ai_protocol = 0; // specify the protocol the socket is gonna be used
                           // for. Useless here. see the list in /etc/protocol

    int gai_status;
    if((gai_status = getaddrinfo(query.node_name, query.service, &hints, &res->first_)) != 0)
    {
        fprintf(stderr, "getaddrinfo error: %s\n", gai_strerror(gai_status));
        // errno = EPERM;
        // errno = gai_status;
        free(res);
        return NULL;
    }

    res->it_ = res->first_;

    return res;
}

socket_Resolver_t* socket_ResolverNext(socket_Resolver_t* it)
{
    if(it == NULL || it->it_ == NULL || it->first_ == NULL)
    {
        errno = EFAULT;
        return NULL;
    }

    if((it->it_ = it->it_->ai_next) == NULL)
    {
        return NULL;
    }

    return it;
}

socket_Endpoint_t* socket_ResolverGetEndpoint(const socket_Resolver_t* it)
{
    if(it == NULL || it->it_ == NULL || it->first_ == NULL)
    {
        errno = EFAULT;
        return NULL;
    }

    return (socket_Endpoint_t*)it->it_->ai_addr;
}

void socket_FreeResolver(socket_Resolver_t* resolver)
{
    if(resolver == NULL || resolver->first_ == NULL)
    {
        errno = EFAULT;
        return;
    }

    freeaddrinfo(resolver->first_);
    // TODO, make choice regarding the following line, for instance if an arrey of resolver exists
    free(resolver);
}
