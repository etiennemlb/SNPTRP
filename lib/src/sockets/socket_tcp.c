/**
 * @file socket_tcp.c
 * @author Etienne Malaboeuf
 * @brief 
 * @version 0.1
 * 
    Simple Non Pragmatic Token Ring Protocol
    Copyright (C) 2020  Etienne Malaboeuf

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

 * 
 */

#include "socket_tcp.h"

#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

/* RESOLVERS */

socket_TCPResolver_t* socket_CreateTCPResolver(socket_Query_t query)
{
    return socket_CreateResolver(query, ENUM_SOCKET_TYPE_STREAM, ENUM_SOCKET_RESOLVER_FLAGS_PASSIVE);
}

socket_TCPResolver_t* socket_TCPResolverNext(socket_TCPResolver_t* it)
{
    return socket_ResolverNext(it);
}

socket_Endpoint_t* socket_TCPResolverGetEndpoint(const socket_TCPResolver_t* it)
{
    return socket_ResolverGetEndpoint(it);
}

void socket_FreeTCPResolver(socket_TCPResolver_t* resolver)
{
    socket_FreeResolver(resolver);
}

/* TCPSOCKET */

socket_TCPSocket_t socket_CreateTCPSocket(const socket_TCPResolver_t* it)
{
    if(it == NULL || it->it_ == NULL || it->first_ == NULL)
    {
        errno = EFAULT;
        return -1;
    }
    socket_TCPSocket_t sock;

    socket_ResolverEndpointInfo_t__* endp = it->it_;

    if((sock = socket(endp->ai_family, endp->ai_socktype, endp->ai_protocol)) < 0)
    {
        return -1;
    }

    if(connect(sock, endp->ai_addr, endp->ai_addrlen) < 0)
    {
        socket_CloseTCPSocket(sock);
        return -1;
    }

    return sock;
}

int socket_TCPGetDestEndpoint(socket_TCPSocket_t sock, socket_Endpoint_t* endp)
{
    return socket_GetDestEndpoint(sock, endp);
}

int socket_CloseTCPSocket(socket_TCPSocket_t sock)
{
    return close(sock);
}

int socket_TCPReceive(socket_TCPSocket_t sock, void* buf, size_t count, size_t* bytes_read, socket_SockIOFlags_e flags)
{
    if(bytes_read == NULL)
    {
        errno = EFAULT;
        return -1;
    }

    size_t  total = 0;
    ssize_t bytes_recved;

    while(total < count)
    {
        bytes_recved = recv(sock, ((uint8_t*)buf) + total, count - total, flags);
        if(bytes_recved == 0)
        {
            *bytes_read = total;
            return 0; /* EOF, socket closed by dest endpoint */
        }

        if(bytes_recved < 0)
        {
            *bytes_read = total;
            return -1; /* localy detected error */
        }

        total += bytes_recved;
    }
    *bytes_read = total;
    return 0;
}

int socket_TCPSend(socket_TCPSocket_t   sock,
                   const void*          buf,
                   size_t               len,
                   size_t*              bytes_written,
                   socket_SockIOFlags_e flags)
{
    if(bytes_written == NULL)
    {
        errno = EFAULT;
        return -1;
    }

    size_t total = 0;

    while(total < len)
    {
        ssize_t bytes_sent = send(sock, ((uint8_t*)buf) + total, len - total, flags);
        if(bytes_sent < 0)
        {
            *bytes_written = total;
            return -1; /* localy detected error */
        }
        total += bytes_sent;
    }
    *bytes_written = total;
    return 0;
}

ssize_t socket_TCPReceiveSome(socket_TCPSocket_t sock, void* buf, size_t count, socket_SockIOFlags_e flags)
{
    return recv(sock, buf, count, flags);
}

ssize_t socket_TCPSendSome(socket_TCPSocket_t sock, const void* buf, size_t len, socket_SockIOFlags_e flags)
{
    return send(sock, buf, len, flags);
}

/* ACCEPTORS */

socket_TCPAcceptor_t socket_CreateTCPAcceptor(const socket_TCPResolver_t* it, int backlog, bool reuse_addr)
{
    if(it == NULL || it->it_ == NULL || it->first_ == NULL)
    {
        errno = EFAULT;
        return -1;
    }

    socket_TCPAcceptor_t acceptor;

    socket_ResolverEndpointInfo_t__* endp = it->it_;

    if((acceptor = socket(endp->ai_family, endp->ai_socktype, endp->ai_protocol)) < 0)
    {
        return -1;
    }

    if(reuse_addr)
    {
        int val = 1;
        if(setsockopt(acceptor, SOL_SOCKET, SO_REUSEADDR, &val, sizeof(val)) < 0)
        {
            socket_CloseTCPAcceptor(acceptor);
            return -1;
        }

        if(setsockopt(acceptor, SOL_SOCKET, SO_REUSEPORT, &val, sizeof(val)) < 0)
        {
            socket_CloseTCPAcceptor(acceptor);
            return -1;
        }
    }

    if(bind(acceptor, endp->ai_addr, endp->ai_addrlen) < 0)
    {
        socket_CloseTCPAcceptor(acceptor);
        return -1;
    }

    if(listen(acceptor, backlog) < 0)
    {
        socket_CloseTCPAcceptor(acceptor);
        return -1;
    }

    /* memcpy(&acceptor->local_endp, endp->ai_addr, endp->ai_addrlen); */

    return acceptor;
}

socket_TCPSocket_t socket_TCPAccept(socket_TCPAcceptor_t acceptor, socket_Endpoint_t* from)
{
    socklen_t addr_len;
    return accept(acceptor, (struct sockaddr*)from, &addr_len);
}

int socket_CloseTCPAcceptor(socket_TCPAcceptor_t acceptor)
{
    //  int shutdown(int sockfd, int how); maybe ?

    return close(acceptor);
}
