/**
 * @file snptrp.c
 * @author Etienne Malaboeuf
 * @brief 
 * @version 0.1
 * 
    Simple Non Pragmatic Token Ring Protocol
    Copyright (C) 2020  Etienne Malaboeuf

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

 * 
 */

#include "snptrp.h"

#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>

#include "debug/debug.h"

int snptrp_InitCtx(snptrp_Context_t* ctx, char* local, char* dest, char* from, char* port_listen, char* port_send)
{
    if(ctx == NULL || local == NULL || dest == NULL || from == NULL || port_listen == NULL || port_send == NULL)
    {
        errno = EFAULT;
        return -1;
    }

    memset(ctx, 0, sizeof(snptrp_Context_t));

    socket_UDPResolver_t* resolver;

    socket_Query_t query = {ENUM_SOCKET_FAMILY_IPV4, local, port_listen};

    // Resolving local addr and bind to it.
    {
        if((resolver = socket_CreateUDPResolver(query)) == NULL)
        {
            return -1;
        }

        // memcpy(&ctx->local_addr_, socket_UDPResolverGetEndpoint(resolver), sizeof(socket_Endpoint_t));

        if((ctx->listening_sock_ = socket_CreateUDPSocket(resolver, true)) < 0)
        {
            socket_FreeUDPResolver(resolver);
            return -1;
        }

        ctx->listening_sock_as_network_ =
            ((struct sockaddr_in*)socket_UDPResolverGetEndpoint(resolver))->sin_addr.s_addr;

        socket_FreeUDPResolver(resolver);
    }

    // Resolving next addr and connecting to it.
    {
        query.service = port_send;
        if((resolver = socket_CreateUDPResolver(query)) == NULL)
        {
            return -1;
        }

        // it matters form where the packet is sent, so just a connected socket is not enough.
        // We need to bind it to the desired local addr.
        if((ctx->next_sock_ = socket_CreateUDPSocket(resolver, true)) < 0)
        {
            socket_FreeUDPResolver(resolver);
            snptrp_DeInitCtx(ctx);
            return -1;
        }

        socket_FreeUDPResolver(resolver);

        query.node_name = dest;
        query.service   = port_listen;

        if((resolver = socket_CreateUDPResolver(query)) == NULL)
        {
            return -1;
        }

        // memcpy(&ctx->next_addr_, socket_UDPResolverGetEndpoint(resolver), sizeof(socket_Endpoint_t));

        if(socket_UDPConnect(ctx->next_sock_, resolver) < 0)
        {
            socket_FreeUDPResolver(resolver);
            snptrp_DeInitCtx(ctx);
            return -1;
        }

        socket_FreeUDPResolver(resolver);
    }

    // Resolving prev addr and connection listening_sock_ to the endp.
    {
        query.node_name = from;
        query.service   = port_send;

        if((resolver = socket_CreateUDPResolver(query)) == NULL)
        {
            snptrp_DeInitCtx(ctx);
            return -1;
        }

        // memcpy(&ctx->prev_addr_, socket_UDPResolverGetEndpoint(resolver), sizeof(socket_Endpoint_t));

        if(socket_UDPConnect(ctx->listening_sock_, resolver) < 0)
        {
            socket_FreeUDPResolver(resolver);
            snptrp_DeInitCtx(ctx);
            return -1;
        }

        socket_FreeUDPResolver(resolver);
    }

    /* fifo TO upper layers in */
    if((ctx->fifo_to_upper_layers_snptp_ = (fifo_t)malloc(sizeof(struct fifo_descriptor))) == NULL)
    {
        snptrp_DeInitCtx(ctx);
        return -1;
    }

    if((ctx->fifo_buffer_to_upper_layers_ =
            (Payload8_t*)malloc(sizeof(snptrp_SegmentHolder_t) * SNPTRP_GLOBAL_SEGMENT_BUFFER_BACKLOG)) == NULL)
    {
        snptrp_DeInitCtx(ctx);
        return -1;
    }

    fifo_create_static(ctx->fifo_to_upper_layers_snptp_,
                       ctx->fifo_buffer_to_upper_layers_,
                       SNPTRP_GLOBAL_SEGMENT_BUFFER_BACKLOG,
                       sizeof(snptrp_SegmentHolder_t));

    /* fifo FROM upper layers in */
    if((ctx->fifo_from_upper_layers_ = (fifo_t)malloc(sizeof(struct fifo_descriptor))) == NULL)
    {
        snptrp_DeInitCtx(ctx);
        return -1;
    }

    if((ctx->fifo_buffer_from_upper_layers_ =
            (Payload8_t*)malloc(sizeof(snptrp_SegmentHolder_t) * SNPTRP_GLOBAL_SEGMENT_BUFFER_BACKLOG)) == NULL)
    {
        snptrp_DeInitCtx(ctx);
        return -1;
    }

    fifo_create_static(ctx->fifo_from_upper_layers_,
                       ctx->fifo_buffer_from_upper_layers_,
                       SNPTRP_GLOBAL_SEGMENT_BUFFER_BACKLOG,
                       sizeof(snptrp_SegmentHolder_t));

    if((ctx->frame_buff_in_ = (Payload8_t*)malloc(SNPTRP_MAX_FRAME_LEN)) == NULL)
    {
        snptrp_DeInitCtx(ctx);
        return -1;
    }

    if((ctx->frame_buff_hdr_out_ = (Payload8_t*)malloc(SNPTRP_MAX_FRAME_LEN)) == NULL) // too much mem but ok
    {
        snptrp_DeInitCtx(ctx);
        return -1;
    }

    if((ctx->frame_buff_payload_out_ = (Payload8_t*)malloc(SNPTRP_MAX_FRAME_LEN)) == NULL) // too much mem but ok
    {
        snptrp_DeInitCtx(ctx);
        return -1;
    }

    pthread_mutex_init(&ctx->mtx_to_upper_layers_snptp_, NULL);
    pthread_mutex_init(&ctx->mtx_from_upper_layers_, NULL);

    return 0;
}

int snptrp_ProtoProcessLoop(snptrp_Context_t* ctx)
{
    // we hope in the function, that no corking happens.
    // see (applis for tcp and udp)
    // https://stackoverflow.com/questions/27139807/what-is-tcp-autocorking-tcp-automatic-corking

    ssize_t bytes_read;

    size_t offset_buff_in         = 0;
    size_t offset_smallheaders_in = SNPTRP_HEADER_LEN;
    size_t offset_data_in         = 0; // We dont know the offset for now. Once the frame is read, we will.

    size_t offset_smallheaders_out = SNPTRP_HEADER_LEN;
    size_t offset_data_out         = 0;

    uint8_t owned_segments_in_frame = 0; // if there is already a payload owned by
                                         // this machine (not a broadcast), it can't
                                         // send. Could be generalized with a counter
                                         // and a treshold of N segment in the frame.

    snptrp_FrameHeader_t* frame_hdr = (snptrp_FrameHeader_t*)ctx->frame_buff_in_;

    if(ctx->original_)
    {
        // send and return
        snptrp_FrameHeader_t frame_header;
        memset(&frame_header, 0, SNPTRP_HEADER_LEN);

        frame_header.preamble_     = SNPTRP_FRAME_PREAMBLE;
        frame_header.header_count_ = 0;
        frame_header.frame_length_ = SNPTRP_HEADER_LEN;

        size_t bytes_written; // dont actually care, guaranteed to be sent execpt if there is an error (then we panic).

        if(socket_UDPSend(ctx->next_sock_, &frame_header, SNPTRP_HEADER_LEN, &bytes_written, 0) < 0)
        {
            // fatal error ???
            // resend a free token ? crash ?


            assert((((void)"Could not socket_UDPSend as the original token."), 0));
            return -1;
        }

        dbprintf("Sent the initial token\n");

        ctx->original_ = 0;
        return 0;
    }

    if((bytes_read = socket_UDPReceiveSome(ctx->listening_sock_, ctx->frame_buff_in_, SNPTRP_MAX_FRAME_LEN, 0)) < 0)
    {

        // fatal error ???
        // resend a free token ? crash ?

        assert((((void)"Could not socket_UDPReceiveSome."), 0));
        return -1;
    }

    // dbprintf("Got something (%ld bytes): \n %s\n.", bytes_read, ctx->frame_buff_in_);

    offset_buff_in += bytes_read;

    if(offset_buff_in < SNPTRP_HEADER_LEN)
    { // We read smthing but it was not long enough to be at least a header.

        // fatal error ???
        // resend a free token ? crash ?

        return -1;
    }

    if(frame_hdr->preamble_ != SNPTRP_FRAME_PREAMBLE)
    { // this preamble allows us to be raisonably sure we are not reading crap
        // (maybe not long enough).

        // fatal error ???
        // resend a free token ? crash ?

        return -1;
    }

    if(frame_hdr->frame_length_ > SNPTRP_MAX_FRAME_LEN)
    { // Too big of a frame.

        // TODO, trunc the frame and continue.

        return -1;
    }

    // dbprintf("Frame length: %d Nb subheader: %d.\n", frame_hdr->frame_length_, frame_hdr->header_count_);

    if(frame_hdr->header_count_ == 0)
    { // no payload, jump to frame forging.

        goto handle_send;
    }

    // dbprintf("Receiving the next bytes.\n");
    if(socket_UDPReceive(ctx->listening_sock_,
                         ctx->frame_buff_in_ + offset_buff_in,
                         frame_hdr->frame_length_ - offset_buff_in,
                         (size_t*)&bytes_read,
                         0) < 0)
    {

        // fatal error ???
        // resend a free token ? crash ?

        assert((((void)"Could not socket_UDPReceive."), 0));
        return -1;
    }

    // dbprintf("5__%zd bytes read.\n", bytes_read);

    offset_buff_in += bytes_read;

    if(offset_buff_in != frame_hdr->frame_length_)
    {

        // we didnt recv the right amount of byte.

        // fatal error ???
        // resend a free token ? crash ?

        return -1;
    }

    // this below, needs optimisation to limit memory copies.

    offset_data_in = SNPTRP_HEADER_LEN + frame_hdr->header_count_ * SNPTRP_SMALLHEADER_LEN;

    snptrp_SmallHeader_t* small_headers = (snptrp_SmallHeader_t*)(ctx->frame_buff_in_ + SNPTRP_HEADER_LEN);

    dbprintf("<- From the cable.");
    for(u_int16_t j = 0; j < frame_hdr->frame_length_ - SNPTRP_HEADER_LEN; j += 1)
    {
        if(j % 4 == 0)
        {
            dbprintf("\n");
        }
        dbprintf("%x ", *((uint8_t*)ctx->frame_buff_in_ + SNPTRP_HEADER_LEN + j));
    }
    dbprintf("\n<-\n");

    for(size_t i = 0; i < frame_hdr->header_count_; ++i)
    {
        dbprintf("Processing small header no %zd: proto:%d, len:%d, dest:%x, src:%x\n",
                 i,
                 small_headers[i].protocol_,
                 small_headers[i].payload_lenght_,
                 small_headers[i].to_,
                 small_headers[i].from_);

        if(small_headers[i].from_ == ctx->listening_sock_as_network_ &&
           small_headers[i].to_ != ctx->listening_sock_as_network_)
        { // the small header is from this local endpoint. But not for this local
            // endp.

            dbprintf("We got a small header that did a full loop of the ring.\n");

            if(small_headers[i].to_ == SNPTRP_BROADCAST_ADDR)
            { // from this machine and dest is broadcast

                // remove this payload because it already
                // aka do nothing
                dbprintf("It was a broadcast, goes straight to the bin.\n");
            }
            else
            { // if there is too much of segment owned by this machine, it will
              // stop sending (adding more segment). but it will still send the already
              // present segments.

                // printf("511__It was a non removed payload owned by this machine. Let it be.\n");

                dbprintf("It was a non removed payload owned by this machine. Goes to the bin.\n");
                // TODO what to do if a small header did a full run of the ring?
                // 1. Count the number of times it did a full run and remove it if it exceed a treshold.
                // 2. Remove it anyway.
                // 3. Let it be infinitely.

                // ++owned_segments_in_frame;

                // memcpy(ctx->frame_buff_hdr_out_ + offset_smallheaders_out,
                //        ctx->frame_buff_in_ + offset_smallheaders_in,
                //        SNPTRP_SMALLHEADER_LEN);
                // memcpy(ctx->frame_buff_payload_out_ + offset_data_out,
                //        ctx->frame_buff_in_ + offset_data_in,
                //        small_headers[i].payload_lenght_);

                // offset_smallheaders_out += SNPTRP_SMALLHEADER_LEN;
                // offset_data_out += small_headers[i].payload_lenght_;
            }
        }
        else if(small_headers[i].to_ == ctx->listening_sock_as_network_)
        { // message is for us.

            dbprintf("We got a small header destined to us.\n");

            snptrp_SegmentHolder_t holder;

            holder.addr_ = small_headers[i].from_;
            holder.size_ = small_headers[i].payload_lenght_;

            if((holder.segment_ = malloc(holder.size_)) == NULL)
            {
                // fatal error ???
                // resend a free token ? crash ?
                assert((((void)"Could not malloc a new segment."), 0));
                return -1;
            }

            // Currently not implemented:
            // A condition that redirect the payload according to the field
            // small_headers[i].protocol_;

            memcpy(holder.segment_, ctx->frame_buff_in_ + offset_data_in, holder.size_);

            pthread_mutex_lock(&ctx->mtx_to_upper_layers_snptp_);

            // potential probleme if the queue is full
            if(!fifo_add(ctx->fifo_to_upper_layers_snptp_, &holder))
            {
                snptrp_DeInitSegmentHolder(&holder);
                pthread_mutex_unlock(&ctx->mtx_to_upper_layers_snptp_);

                // fatal error ???
                // resend a free token ? crash ?
                assert((((void)"Could not fifo_add."), 0));
                return -1;
            }

            dbprintf("Forwarded to upper layers.\n");

            pthread_mutex_unlock(&ctx->mtx_to_upper_layers_snptp_);
        }
        else
        { // copy a segment and its header to the out buffer because we are not
            // it's destination.

            dbprintf("We got a small header which is not destined to us. Let it be.\n");

            // dbprintf("<-");
            // for(int j = 0; j < SNPTRP_SMALLHEADER_LEN; j += 1)
            // {
            //     if(j % 4 == 0)
            //     {
            //         dbprintf("\n");
            //     }
            //     dbprintf("%x ", *((uint8_t*)ctx->frame_buff_in_ + offset_smallheaders_in + j));
            // }
            // dbprintf("\n<-\n");
            // for(int j = 0; j < small_headers[i].payload_lenght_; j += 1)
            // {
            //     if(j % 4 == 0)
            //     {
            //         dbprintf("\n");
            //     }
            //     dbprintf("%x ", *((uint8_t*)ctx->frame_buff_in_ + offset_data_in + j));
            // }
            // dbprintf("\n<-\n");

            memcpy(ctx->frame_buff_hdr_out_ + offset_smallheaders_out,
                   ctx->frame_buff_in_ + offset_smallheaders_in,
                   SNPTRP_SMALLHEADER_LEN);
            memcpy(ctx->frame_buff_payload_out_ + offset_data_out,
                   ctx->frame_buff_in_ + offset_data_in,
                   small_headers[i].payload_lenght_);

            offset_smallheaders_out += SNPTRP_SMALLHEADER_LEN;
            offset_data_out += small_headers[i].payload_lenght_;
        }

        offset_smallheaders_in += SNPTRP_SMALLHEADER_LEN;
        offset_data_in += small_headers[i].payload_lenght_;
    }

handle_send:

    pthread_mutex_lock(&ctx->mtx_from_upper_layers_);

    // send stuff
    while((owned_segments_in_frame <= SNPTRP_MAX_OWNED_SEGMENT_ON_FRAME) &&
          ((offset_smallheaders_out + SNPTRP_SMALLHEADER_LEN + offset_data_out + SNPTRP_MAX_PAYLOAD_UNIT_LEN +
            SNPTRP_MAX_PAYLOAD_UNIT_LEN) < SNPTRP_MAX_FRAME_LEN) &&
          !fifo_is_empty(ctx->fifo_from_upper_layers_))
    { // Can send (can fit a segment, not too much segment in frame) and want to send. Perfect

        // puts("The user want to send.");

        snptrp_SegmentHolder_t holder;

        if(!fifo_get(ctx->fifo_from_upper_layers_, &holder))
        {
            pthread_mutex_unlock(&ctx->mtx_from_upper_layers_);

            // fatal error ???
            // resend a free token ? crash ?
            assert((((void)"Could not fifo_get."), 0));
            return -1;
        }

        dbprintf("<- From Upper.");
        for(size_t j = 0; j < holder.size_; j += 1)
        {
            if(j % 4 == 0)
            {
                dbprintf("\n");
            }
            dbprintf("%x ", *((Payload8_t*)holder.segment_ + j));
        }
        dbprintf("\n<-\n");

        snptrp_SmallHeader_t small_header;
        memset(&small_header, 0, sizeof(small_header));

        small_header.protocol_       = SNPTRP_PROTO_SNPTP; // holder.proto_;
        small_header.from_           = ctx->listening_sock_as_network_;
        small_header.to_             = holder.addr_;
        small_header.payload_lenght_ = holder.size_;


        memcpy(ctx->frame_buff_hdr_out_ + offset_smallheaders_out, &small_header, SNPTRP_SMALLHEADER_LEN);
        memcpy(ctx->frame_buff_payload_out_ + offset_data_out, holder.segment_, small_header.payload_lenght_);

        offset_smallheaders_out += SNPTRP_SMALLHEADER_LEN;
        offset_data_out += small_header.payload_lenght_;

        ++owned_segments_in_frame;
        snptrp_DeInitSegmentHolder(&holder);
    }

    pthread_mutex_unlock(&ctx->mtx_from_upper_layers_);


    snptrp_FrameHeader_t frame_header;
    memset(&frame_header, 0, SNPTRP_HEADER_LEN);

    frame_header.preamble_ = SNPTRP_FRAME_PREAMBLE;

    if(offset_smallheaders_out > SNPTRP_HEADER_LEN)
    {
        frame_header.header_count_ = (offset_smallheaders_out - SNPTRP_HEADER_LEN) / SNPTRP_SMALLHEADER_LEN;
    }
    else
    {
        frame_header.header_count_ = 0;
    }

    // frame_header.flags_1_ = 0x12;

    frame_header.frame_length_ =
        SNPTRP_HEADER_LEN + frame_header.header_count_ * SNPTRP_SMALLHEADER_LEN + offset_data_out;

    // dbprintf("Sending %d small headers and %d bytes of payload.\n", frame_header.header_count_, offset_data_out);

    memcpy(ctx->frame_buff_hdr_out_, &frame_header, SNPTRP_HEADER_LEN);

    size_t bytes_written; // dont actually care, guaranteed to be sent execpt if there is an error (then we panic).

    if(socket_UDPSend(ctx->next_sock_, ctx->frame_buff_hdr_out_, offset_smallheaders_out, &bytes_written, 0) < 0)
    {
        // fatal error ???
        // resend a free token ? crash ?
        assert((((void)"Could not socket_UDPSend."), 0));
        return -1;
    }

    // dbprintf("Sending %ld bytes of header.\n", offset_smallheaders_out);

    if(socket_UDPSend(ctx->next_sock_, ctx->frame_buff_payload_out_, offset_data_out, &bytes_written, 0) < 0)
    {
        // fatal error ???
        // resend a free token ? crash ?
        assert((((void)"Could not socket_UDPSend."), 0));
        return -1;
    }

    // dbprintf("Sending %ld bytes of data.\n", offset_data_out);

    return 0;
}

void snptrp_DeInitCtx(snptrp_Context_t* ctx)
{
    if(ctx == NULL)
    {
        errno = EFAULT;
        return;
    }

    pthread_mutex_lock(&ctx->mtx_to_upper_layers_snptp_);
    pthread_mutex_lock(&ctx->mtx_from_upper_layers_);

    if(ctx->fifo_from_upper_layers_)
    {
        free(ctx->fifo_from_upper_layers_);
    }

    if(ctx->fifo_buffer_from_upper_layers_)
    {
        free(ctx->fifo_buffer_from_upper_layers_);
    }

    if(ctx->fifo_to_upper_layers_snptp_)
    {
        free(ctx->fifo_to_upper_layers_snptp_);
    }

    if(ctx->fifo_buffer_to_upper_layers_)
    {
        free(ctx->fifo_buffer_to_upper_layers_);
    }

    if(ctx->frame_buff_in_)
    {
        free(ctx->frame_buff_in_);
    }

    if(ctx->frame_buff_payload_out_)
    {
        free(ctx->frame_buff_payload_out_);
    }

    if(ctx->frame_buff_hdr_out_)
    {
        free(ctx->frame_buff_hdr_out_);
    }

    if(ctx->listening_sock_)
    {
        socket_CloseUDPSocket(ctx->listening_sock_);
    }

    if(ctx->next_sock_)
    {
        socket_CloseUDPSocket(ctx->next_sock_);
    }

    pthread_mutex_lock(&ctx->mtx_to_upper_layers_snptp_);
    pthread_mutex_destroy(&ctx->mtx_to_upper_layers_snptp_);
    pthread_mutex_lock(&ctx->mtx_from_upper_layers_);
    pthread_mutex_destroy(&ctx->mtx_from_upper_layers_);
}

int snptrp_SetOriginal(snptrp_Context_t* ctx)
{
    if(ctx == NULL)
    {
        errno = EFAULT;
        return -1;
    }

    ctx->original_ = 1;

    return 0;
}

void snptrp_DeInitSegmentHolder(snptrp_SegmentHolder_t* holder)
{
    if(holder == NULL)
    {
        return;
    }

    if(holder->segment_ != NULL)
    {
        free(holder->segment_);
    }
}
