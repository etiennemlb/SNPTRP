/**
 * @file snptp.c
 * @author Etienne Malaboeuf
 * @brief 
 * @version 0.1
 * 
    Simple Non Pragmatic Token Ring Protocol
    Copyright (C) 2020  Etienne Malaboeuf

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

 * 
 */

/* #include <arpa/inet.h> */
#include <assert.h>
#include <errno.h>
#include <pthread.h>
#include <sockets/socket.h>
#include <stdlib.h>
#include <string.h>

/* #ifndef __STDC_NO_ATOMICS__
#include <stdatomic.h>
#endif */

#include "crc32.h"
#include "debug/debug.h"
#include "snptp.h"
#include "snptrp.h"
#include "types.h"

/* Private */

size_t Clamp(size_t val, size_t max)
{
    return val > max ? max : val;
}

int snptp_CheckSumValid(uint32_t crc, snptp_Segment_t* segment)
{
    if(segment == NULL)
    {
        errno = EFAULT;
        return 0; // bool
    }

    return crc == snptrp_CRC32Compute(0, (unsigned char*)segment, segment->hdr_.segment_length_);
}

/**
 * @brief Use snptrp_DeInitSegmentHolder() to deinit the allocated memory.
 *
 * @param holder
 * @param hdr
 * @param dest
 * @param data
 * @param len
 * @return int
 */
int snptp_ForgeSegment(snptrp_SegmentHolder_t* holder,
                       snptp_SegmentHeader_t   hdr,
                       Address32_t             dest,
                       Payload8_t*             data,
                       size_t                  len)
{
    if(holder == NULL)
    {
        errno = EFAULT;
        return -1;
    }

    // just send ctrl flags
    if(data == NULL)
    {
        len = 0;
    }

    holder->addr_ = dest;
    holder->size_ = SNPTP_HEADER_LEN + len;

    // only set those two fields, the rest is left to the caller.

    hdr.check_sum_      = 0; // for the crc.
    hdr.segment_length_ = holder->size_;

    Payload8_t* segment_hdr_data;
    if((segment_hdr_data = (Payload8_t*)malloc(holder->size_)) == NULL)
    {
        return -1;
    }

    memcpy(segment_hdr_data, &hdr, sizeof(hdr));
    memcpy(segment_hdr_data + sizeof(hdr), data, len);

    uint32_t crc = snptrp_CRC32Compute(0, (unsigned char*)segment_hdr_data, holder->size_);

    ((snptp_SegmentHeader_t*)segment_hdr_data)->check_sum_ = crc;

    holder->segment_ = segment_hdr_data;
    return 0;
}

int snptp_SendToLower(snptp_Context_t*      ctx,
                      snptp_SegmentHeader_t hdr,
                      Payload8_t*           data,
                      size_t                len,
                      Address32_t           addr_to)
{
    if(ctx == NULL)
    {
        errno = EFAULT;
        return -1;
    }

    snptrp_SegmentHolder_t holder;

    if(snptp_ForgeSegment(&holder, hdr, addr_to, data, len) < 0)
    {
        return -1;
    }

    pthread_mutex_lock(ctx->global_out_mtx_);
    // potential probleme if the queue is full
    if(!fifo_add(ctx->global_out_buffer_snptrp_, &holder))
    {
        pthread_mutex_unlock(ctx->global_out_mtx_);
        snptrp_DeInitSegmentHolder(&holder);
        errno = ENOSPC;
        return -1;
    }
    pthread_mutex_unlock(ctx->global_out_mtx_);

    return 0;
}

/* Public function */

int snptp_InitCtx(snptp_Context_t* ctx, snptrp_Context_t* snptrp_ctx)
{
    if(ctx == NULL || snptrp_ctx == NULL)
    {
        errno = EFAULT;
        return -1;
    }

    memset(ctx, 0, sizeof(*ctx));

    ctx->global_in_buffer_snptrp_  = snptrp_ctx->fifo_to_upper_layers_snptp_;
    ctx->global_out_buffer_snptrp_ = snptrp_ctx->fifo_from_upper_layers_;

    ctx->global_in_mtx_  = &snptrp_ctx->mtx_to_upper_layers_snptp_;
    ctx->global_out_mtx_ = &snptrp_ctx->mtx_from_upper_layers_;


    map_init(&ctx->map_endpoint_);
    pthread_mutex_init(&ctx->map_endpoint_mtx_, NULL);

    return 0;
}

int snptp_ProtoProcessLoop(snptp_Context_t* ctx)
{
#define SNPTP_PROCESS_LOOP_EXIT_CLEAN_NOW(msg)                                                                         \
    snptrp_DeInitSegmentHolder(&segment_in_from_lower_layer);                                                          \
    pthread_mutex_unlock(&ctx->map_endpoint_mtx_);                                                                     \
    pthread_mutex_unlock(ctx->global_in_mtx_);                                                                         \
    assert((((void)msg), 0));                                                                                                                                                                                                   \
    return -1;

    // créer les snptrp_SegmentHolder_t et forward à snptrp.
    pthread_mutex_lock(ctx->global_in_mtx_);

    while(!fifo_is_empty(ctx->global_in_buffer_snptrp_))
    { // We got segment to process

        snptrp_SegmentHolder_t segment_in_from_lower_layer;
        if(!fifo_get(ctx->global_in_buffer_snptrp_, &segment_in_from_lower_layer))
        {
            // fatal error.
            pthread_mutex_unlock(ctx->global_in_mtx_);
            errno = EFAULT;

            assert((((void)"Could not fifo_get."), 0));
            return -1;
        }

        dbprintf("-> New segment.");
        for(size_t j = 0; j < segment_in_from_lower_layer.size_; j += 1)
        {
            if(j % 4 == 0)
            {
                dbprintf("\n");
            }
            dbprintf("%x ", *((Payload8_t*)segment_in_from_lower_layer.segment_ + j));
        }
        dbprintf("\n->\n");

        snptp_Segment_t* segment_in_ptr = (snptp_Segment_t*)segment_in_from_lower_layer.segment_;
        uint32_t         crc            = segment_in_ptr->hdr_.check_sum_;
        segment_in_ptr->hdr_.check_sum_ = 0;

        int checksum_valid              = snptp_CheckSumValid(crc, segment_in_ptr);
        segment_in_ptr->hdr_.check_sum_ = crc;

        char addr_as_str[SOCKET_IPV4_ADDR_STR_LEN];
        if(socket_IPV4ToStr(segment_in_from_lower_layer.addr_, addr_as_str) < 0)
        {
            // error.
            snptrp_DeInitSegmentHolder(&segment_in_from_lower_layer);
            continue;
        }

        pthread_mutex_lock(&ctx->map_endpoint_mtx_);
        snptp_EndpointCtx_t** endp_ctx_ptr_ptr = map_get(&ctx->map_endpoint_, addr_as_str);

        dbprintf("-> Got a segment from %s.\n", addr_as_str);

        if(endp_ctx_ptr_ptr == NULL) // check if the map contains a value for a key
        {
            dbprintf("-> Unknown peer.\n");

            if(!checksum_valid)
            { // unknown and invalid checksum, nothing to be done, next.
                snptrp_DeInitSegmentHolder(&segment_in_from_lower_layer);
                pthread_mutex_unlock(&ctx->map_endpoint_mtx_);

                dbprintf("-> Invalid crc: continuing.\n");
                continue;
            }

            dbprintf("-> Valid crc.\n");

            if(segment_in_ptr->hdr_.flags_syn_ && !segment_in_ptr->hdr_.flags_ack_)
            { // unknown endp, might be a peer asking for a new connection.
                // create new endp in map.
                snptp_EndpointCtx_t* new_endp;

                if((new_endp = (snptp_EndpointCtx_t*)malloc(sizeof(*new_endp))) == NULL)
                {
                    // fatal error.
                    SNPTP_PROCESS_LOOP_EXIT_CLEAN_NOW("Could not malloc new_endp.");
                }

                if(snptp_InitEndpointCtx(new_endp, addr_as_str) < 0)
                {
                    // fatal error.
                    free(new_endp);
                    SNPTP_PROCESS_LOOP_EXIT_CLEAN_NOW("Could not snptp_InitEndpointCtx.");
                }

                if(map_set(&ctx->map_endpoint_, addr_as_str, new_endp) < 0)
                {
                    errno = EPERM;
                    // fatal error.
                    free(new_endp);
                    SNPTP_PROCESS_LOOP_EXIT_CLEAN_NOW("Could not map_set.");
                }

                dbprintf("-> The peer wants to connect.\n");

                new_endp->state_       = SNPTP_STATE_SYN_WAIT;
                new_endp->ack_counter_ = 1;

                snptp_SegmentHeader_t hdr;
                memset(&hdr, 0, sizeof(hdr));

                hdr.flags_ack_ = 1;
                hdr.flags_syn_ = 1;

                if(snptp_SendToLower(ctx, hdr, NULL, 0, segment_in_from_lower_layer.addr_) < 0)
                {
                    // fatal error.
                    free(new_endp);
                    SNPTP_PROCESS_LOOP_EXIT_CLEAN_NOW("Could not snptp_SendToLower.");
                }

                dbprintf("-> Sent syn/ack to lower layers.\n");
            }
            else
            {
                dbprintf("We got trash.\n");
            }


            snptrp_DeInitSegmentHolder(&segment_in_from_lower_layer);
            pthread_mutex_unlock(&ctx->map_endpoint_mtx_);
            continue;
        }

        // handle flags according to the connection state.

        snptp_EndpointCtx_t* endp_ctx_ptr = *endp_ctx_ptr_ptr;

        dbprintf("-> Known peer.\n");

        if(endp_ctx_ptr->state_ == SNPTP_STATE_ESTABLISHED)
        { // if we the connection is established already.

            dbprintf("-> Established, checking for invalid segment.\n");

            if(!checksum_valid)
            { // known endp but invalid crc and ESTABLISHED. Ask for resend.

                dbprintf("-> Invalid checksum.\n");

                // send resend flag
                snptp_SegmentHeader_t hdr;

                memset(&hdr, 0, sizeof(hdr));

                hdr.flags_ask_resend_ = 1; // ask for deco.

                if(snptp_SendToLower(ctx, hdr, NULL, 0, segment_in_from_lower_layer.addr_) < 0)
                {
                    // fatal error.
                    SNPTP_PROCESS_LOOP_EXIT_CLEAN_NOW("Could not snptp_SendToLower.");
                }

                snptrp_DeInitSegmentHolder(&segment_in_from_lower_layer);
                pthread_mutex_unlock(&ctx->map_endpoint_mtx_);
                continue;
            }

            dbprintf("-> Valid checksum.\n");

            if(segment_in_ptr->hdr_.flags_fin_ && !segment_in_ptr->hdr_.flags_ack_)
            { // handle peer asking for deco.

                dbprintf("-> The peer want to close the connection.\n");

                snptp_SegmentHeader_t hdr;

                memset(&hdr, 0, sizeof(hdr));

                hdr.flags_ack_ = 1; // ack the deconnection.
                hdr.flags_fin_ = 1; // ask for deco.

                endp_ctx_ptr->state_       = SNPTP_STATE_FIN_WAIT;
                endp_ctx_ptr->ack_counter_ = 1;

                if(snptp_SendToLower(ctx, hdr, NULL, 0, segment_in_from_lower_layer.addr_) < 0)
                {
                    // fatal error.
                    SNPTP_PROCESS_LOOP_EXIT_CLEAN_NOW("Could not snptp_SendToLower.");
                }

                dbprintf("-> Ack/fin sent to lower layers.\n");

                snptrp_DeInitSegmentHolder(&segment_in_from_lower_layer);
                pthread_mutex_unlock(&ctx->map_endpoint_mtx_);
                continue;
            }
            else if(segment_in_ptr->hdr_.flags_syn_)
            { // incoherent flags with established state. Send fin.
                // ask for DECO.

                dbprintf("-> Peer sent syn, incoherent flag, ask for deco.\n");

                snptp_SegmentHeader_t hdr;

                memset(&hdr, 0, sizeof(hdr));

                // no ack here.
                hdr.flags_fin_ = 1; // ask for deco.

                endp_ctx_ptr->state_       = SNPTP_STATE_FIN_WAIT;
                endp_ctx_ptr->ack_counter_ = 1;

                if(snptp_SendToLower(ctx, hdr, NULL, 0, segment_in_from_lower_layer.addr_) < 0)
                {
                    // fatal error.
                    SNPTP_PROCESS_LOOP_EXIT_CLEAN_NOW("Could not snptp_SendToLower.");
                }

                snptrp_DeInitSegmentHolder(&segment_in_from_lower_layer);
                pthread_mutex_unlock(&ctx->map_endpoint_mtx_);
                continue;
            }
            // fall through, handle other flags (ack stuff).
        } // endp_ctx_ptr->state_ == SNPTP_STATE_ESTABLISHED
        else
        { // != ESTABLISHED.

            dbprintf("-> Not Established, checking for invalid segment.\n");

            // handle connection setuping (not yet established).

            if(!checksum_valid)
            { // known endp but invalid crc and NOT ESTABLISHED. Send FIN.

                dbprintf("-> Invalid checksum.\n");

                // send FIN flag
                snptp_SegmentHeader_t hdr;

                memset(&hdr, 0, sizeof(hdr));

                // no ack here.
                hdr.flags_fin_ = 1; // ask for deco.

                endp_ctx_ptr->state_       = SNPTP_STATE_FIN_WAIT;
                endp_ctx_ptr->ack_counter_ = 1;

                if(snptp_SendToLower(ctx, hdr, NULL, 0, segment_in_from_lower_layer.addr_) < 0)
                {
                    // fatal error.
                    SNPTP_PROCESS_LOOP_EXIT_CLEAN_NOW("Could not snptp_SendToLower.");
                }

                snptrp_DeInitSegmentHolder(&segment_in_from_lower_layer);
                pthread_mutex_unlock(&ctx->map_endpoint_mtx_);
                continue;
            }

            dbprintf("-> Valid checksum.\n");

            // if in synwait or finwait, the next segment comming from the endp mush be ack (maybe with
            // synwait/finwait).

            if(!segment_in_ptr->hdr_.flags_ack_)
            { // No ack.

                if(endp_ctx_ptr->state_ == SNPTP_STATE_FIN_WAIT)
                { // no ack and waiting, error.

                    // delete local endpoint.
                    // nothing to be done, we already sent a fin flag.

                    // CLOSED
                    snptp_DeInitEndpointCtx(endp_ctx_ptr);
                    free(endp_ctx_ptr);
                    map_remove(&ctx->map_endpoint_, addr_as_str);

                    dbprintf("-> Connection CLOSED on this endp. (Hard disconnect).\n");

                } // fall throught.
                else if(endp_ctx_ptr->state_ == SNPTP_STATE_SYN_WAIT)
                { // no ack and waiting, error.

                    // ask for deco.
                    snptp_SegmentHeader_t hdr;

                    memset(&hdr, 0, sizeof(hdr));

                    if(segment_in_ptr->hdr_.flags_fin_)
                    { // answer the peer asking for deco (connection refused).

                        hdr.flags_ack_ = 1; // ack deco
                        hdr.flags_fin_ = 1; // ask for deco.
                    }
                    else
                    { // no ack here, peer fucked up.

                        hdr.flags_fin_ = 1; // ask for deco.
                    }

                    endp_ctx_ptr->state_       = SNPTP_STATE_FIN_WAIT;
                    endp_ctx_ptr->ack_counter_ = 1;

                    if(snptp_SendToLower(ctx, hdr, NULL, 0, segment_in_from_lower_layer.addr_) < 0)
                    {
                        // fatal error.
                        SNPTP_PROCESS_LOOP_EXIT_CLEAN_NOW("Could not snptp_SendToLower.");
                    }
                } // fall throught.
            }
            else
            { // Got ack.

                if(endp_ctx_ptr->state_ == SNPTP_STATE_SYN_WAIT)
                { // waiting for ack and got ack.

                    dbprintf("-> Connection ESTABLISHED on this endp.\n");

                    endp_ctx_ptr->state_       = SNPTP_STATE_ESTABLISHED;
                    endp_ctx_ptr->ack_counter_ = 0;

                    if(segment_in_ptr->hdr_.flags_syn_)
                    { // got ack and syn, so send an ack.

                        // add ack flag and send packet

                        snptp_SegmentHeader_t hdr;

                        memset(&hdr, 0, sizeof(hdr));

                        // no ack here.
                        hdr.flags_ack_ = 1; // ack syn.

                        if(snptp_SendToLower(ctx, hdr, NULL, 0, segment_in_from_lower_layer.addr_) < 0)
                        {
                            // fatal error.
                            SNPTP_PROCESS_LOOP_EXIT_CLEAN_NOW("Could not snptp_SendToLower.");
                        }
                    }
                }
                else if(endp_ctx_ptr->state_ == SNPTP_STATE_FIN_WAIT)
                { // waiting for ack and got ack.

                    endp_ctx_ptr->state_ = SNPTP_STATE_CLOSED;

                    if(segment_in_ptr->hdr_.flags_fin_)
                    { // got ack and fin so send an ack.

                        // add ack flag and send packet.

                        snptp_SegmentHeader_t hdr;

                        memset(&hdr, 0, sizeof(hdr));

                        // no ack here.
                        hdr.flags_ack_ = 1; // ask for deco.

                        if(snptp_SendToLower(ctx, hdr, NULL, 0, segment_in_from_lower_layer.addr_) < 0)
                        {
                            // fatal error.
                            SNPTP_PROCESS_LOOP_EXIT_CLEAN_NOW("Could not snptp_SendToLower.");
                        }
                    }
                    // delete local endpoint. (CLOSED)
                    // CLOSED.
                    snptp_DeInitEndpointCtx(endp_ctx_ptr);
                    free(endp_ctx_ptr);
                    map_remove(&ctx->map_endpoint_, addr_as_str);

                    dbprintf("-> Connection CLOSED on this endp.\n");
                }
                else
                {
                    // fatal error.
                    // but unreachable if not buggy.
                    SNPTP_PROCESS_LOOP_EXIT_CLEAN_NOW("Unreachable code reached O.o .");
                }
                // fall throught.
            }

            snptrp_DeInitSegmentHolder(&segment_in_from_lower_layer);
            pthread_mutex_unlock(&ctx->map_endpoint_mtx_);
            continue;
        } // != ESTABLISHED

        // We are established, and all control flags have been taken care of.

        if(segment_in_ptr->hdr_.flags_ack_ && !segment_in_ptr->hdr_.flags_ask_resend_)
        { // handle ack flag on data

            dbprintf("-> Handling ack. Ack_counter: %u, lastbyte: '%c', lastbyte received: '%c'.\n",
                     endp_ctx_ptr->ack_counter_,
                     endp_ctx_ptr->last_byte_sent_,
                     segment_in_ptr->hdr_.last_byte_);

            if((endp_ctx_ptr->ack_counter_ > 0) & (segment_in_ptr->hdr_.last_byte_ == endp_ctx_ptr->last_byte_sent_))
            { // validate the acknoledgment.
                dbprintf("-> Received a Valid ack. We can now continue sending data to this peer.\n");
                endp_ctx_ptr->ack_counter_ = 0;
            } // fall through cuz the segment might contain data in addition to the ack flag.
            else
            { // the distant peer send something fishy, because the crc is valid, the state is valid but it acknoledged
              // a bad byte.

                // deco
                dbprintf(
                    "-> The distant peer send something fishy, because the crc is valid,\n"
                    "-> the state is valid but it acknoledged a bad byte or acked something that did not need it.\n"
                    "-> Sending FIN flag.\n");

                snptp_SegmentHeader_t hdr;

                memset(&hdr, 0, sizeof(hdr));

                // no ack here.
                hdr.flags_fin_ = 1; // ask for deco.

                endp_ctx_ptr->state_       = SNPTP_STATE_FIN_WAIT;
                endp_ctx_ptr->ack_counter_ = 1;

                if(snptp_SendToLower(ctx, hdr, NULL, 0, segment_in_from_lower_layer.addr_) < 0)
                {
                    // fatal error.
                    SNPTP_PROCESS_LOOP_EXIT_CLEAN_NOW("Could not snptp_SendToLower.");
                }

                snptrp_DeInitSegmentHolder(&segment_in_from_lower_layer);
                pthread_mutex_unlock(&ctx->map_endpoint_mtx_);
                continue;
            }
        }

        if(segment_in_ptr->hdr_.flags_ask_resend_ && !segment_in_ptr->hdr_.flags_ack_)
        { // ask to resend the last segment.
            snptp_SegmentHeader_t hdr;
            size_t                data_len = 0;
            Payload8_t*           data     = NULL;

            dbprintf("-> Handling resend.\n");

            if(endp_ctx_ptr->have_last_segment_)
            { // if there was a last segment.

                // resend.
                hdr      = endp_ctx_ptr->last_sent_segment_.hdr_;
                data_len = endp_ctx_ptr->last_sent_segment_.hdr_.segment_length_ - SNPTP_HEADER_LEN;
                data     = endp_ctx_ptr->last_sent_segment_.data_;
            }
            else
            { // error.
                memset(&hdr, 0, sizeof(hdr));

                hdr.flags_fin_ = 1;

                endp_ctx_ptr->state_       = SNPTP_STATE_FIN_WAIT;
                endp_ctx_ptr->ack_counter_ = 1;
            }

            if(snptp_SendToLower(
                   ctx, hdr, data, data_len, segment_in_from_lower_layer.addr_) < 0)
            {
                // fatal error.
                SNPTP_PROCESS_LOOP_EXIT_CLEAN_NOW("Could not snptp_SendToLower.");
            }

            snptrp_DeInitSegmentHolder(&segment_in_from_lower_layer);
            pthread_mutex_unlock(&ctx->map_endpoint_mtx_);
            continue;
        }

        if((segment_in_ptr->hdr_.segment_length_ - SNPTP_HEADER_LEN) > 0)
        { // We got data

            dbprintf("-> Handling new data.\n");

            if(!endp_ctx_ptr->last_segment_acknoledged_)
            { // we received a new segment while not having acknowledged the previous one.
                // error fatal.

                dbprintf("-> Should not be receiving data (no ack sent).\n");

                snptp_SegmentHeader_t hdr;
                memset(&hdr, 0, sizeof(hdr));

                hdr.flags_fin_ = 1; // ask for deco.

                endp_ctx_ptr->state_       = SNPTP_STATE_FIN_WAIT;
                endp_ctx_ptr->ack_counter_ = 1;

                if(snptp_SendToLower(ctx, hdr, NULL, 0, endp_ctx_ptr->endpoint_) < 0)
                {
                    // fatal error.
                    errno = EPROTO;
                    SNPTP_PROCESS_LOOP_EXIT_CLEAN_NOW("Could not snptp_SendToLower.");
                    return -1;
                }

                continue;
            }

            // potential probleme if the queue is full
            if(!fifo_add(endp_ctx_ptr->segment_in_, &segment_in_ptr))
            {
                // fatal error.
                errno = ENOSPC;
                SNPTP_PROCESS_LOOP_EXIT_CLEAN_NOW("Could not snptp_SendToLower.");
            }

            // store the last byte received for future ack.
            endp_ctx_ptr->last_byte_received_ =
                segment_in_ptr->data_[segment_in_ptr->hdr_.segment_length_ - SNPTP_HEADER_LEN - 1];

            endp_ctx_ptr->last_segment_acknoledged_ = 0;

            // dont deinit the segment holder. We need the data.
            pthread_mutex_unlock(&ctx->map_endpoint_mtx_);
            continue;
        }
        else if(segment_in_ptr->hdr_.flags_ack_ && !segment_in_ptr->hdr_.flags_ask_resend_)
        { // catch potential "ack only" segment.

            snptrp_DeInitSegmentHolder(&segment_in_from_lower_layer);
            pthread_mutex_unlock(&ctx->map_endpoint_mtx_);
            continue;
        }
        errno = EPROTO;
        // If we et here, there was a bug
        SNPTP_PROCESS_LOOP_EXIT_CLEAN_NOW("Unreachable code reached.");
    }

    /* pthread_mutex_unlock(&ctx->map_endpoint_mtx_); */
    pthread_mutex_unlock(ctx->global_in_mtx_);

    map_iter_t  iter = map_iter();
    const char* key;

    pthread_mutex_lock(&ctx->map_endpoint_mtx_);

    while((key = map_next(&ctx->map_endpoint_, &iter)) != NULL)
    { // for all current connection.
        snptp_EndpointCtx_t** endp_ctx_ptr_ptr;
        snptp_EndpointCtx_t*  endp_ctx_ptr;

        if((endp_ctx_ptr_ptr = map_get(&ctx->map_endpoint_, key)) == NULL)
        {
            // fatal error.
            pthread_mutex_unlock(&ctx->map_endpoint_mtx_);
            errno = EFAULT;

            assert((((void)"Could not map_get."), 0));
            return -1;
        }

        endp_ctx_ptr = *endp_ctx_ptr_ptr;

        if(endp_ctx_ptr->state_ == SNPTP_STATE_ESTABLISHED)
        { // Is in a state that allows sending data.

            if(!fifo_is_empty(endp_ctx_ptr->segment_out_))
            { // user want to send

                dbprintf("-> The user want to send to: %s.\n", key);

                if(endp_ctx_ptr->ack_counter_ == 0)
                { // User can send because the last segment has been acknowledged.
                    snptp_Segment_t* segment_to_be_sent;

                    dbprintf("-> And he can(all prev segments are acked).\n");

                    if(!fifo_get(endp_ctx_ptr->segment_out_, &segment_to_be_sent))
                    {
                        // fatal error.
                        pthread_mutex_unlock(&ctx->map_endpoint_mtx_);
                        free(segment_to_be_sent);
                        errno = EFAULT;

                        assert((((void)"Could not fifo_get."), 0));
                        return -1;
                    }

                    snptrp_SegmentHolder_t holder;
                    snptp_SegmentHeader_t  hdr;

                    memset(&hdr, 0, sizeof(hdr));

                    if(!endp_ctx_ptr->last_segment_acknoledged_)
                    { // send data with an ack, we did not consume the last byte received.

                        hdr.flags_ack_ = 1; // ack the deconnection.
                        hdr.last_byte_ = endp_ctx_ptr->last_byte_received_;

                        endp_ctx_ptr->last_segment_acknoledged_ = 1;
                    }

                    if(snptp_ForgeSegment(&holder,
                                          hdr,
                                          endp_ctx_ptr->endpoint_,
                                          (Payload8_t*)segment_to_be_sent + SNPTP_HEADER_LEN,
                                          segment_to_be_sent->hdr_.segment_length_ - SNPTP_HEADER_LEN) < 0)
                    {

                        pthread_mutex_unlock(&ctx->map_endpoint_mtx_);
                        free(segment_to_be_sent);
                        errno = EFAULT;

                        assert((((void)"Could not snptp_ForgeSegment."), 0));
                        return -1;
                    }

                    endp_ctx_ptr->ack_counter_ = 1; // this new segment we are sending needs to be acked.

                    pthread_mutex_lock(ctx->global_out_mtx_);

                    // potential probleme if the queue is full
                    if(!fifo_add(ctx->global_out_buffer_snptrp_, &holder))
                    {
                        free(segment_to_be_sent);

                        pthread_mutex_unlock(&ctx->map_endpoint_mtx_);
                        pthread_mutex_unlock(ctx->global_out_mtx_);
                        snptrp_DeInitSegmentHolder(&holder);
                        errno = ENOSPC;

                        assert((((void)"Could not fifo_add."), 0));
                        return -1;
                    }

                    pthread_mutex_unlock(ctx->global_out_mtx_);

                    // save the last segment
                    memcpy(&endp_ctx_ptr->last_sent_segment_, holder.segment_, holder.size_);
                    endp_ctx_ptr->last_byte_sent_ =
                        ((Payload8_t*)holder.segment_)[holder.size_ - 1]; // store for future ack validation.

                    endp_ctx_ptr->have_last_segment_ = 1; // we sent something

                    free(segment_to_be_sent);
                }

                continue;
            }
            else
            { // the user dont want to send data


                if(!endp_ctx_ptr->last_segment_acknoledged_)
                { // CHECK if we got to send an ack.

                    dbprintf("-> The user dont want to send but got to ack.\n");

                    snptp_SegmentHeader_t hdr;
                    memset(&hdr, 0, sizeof(hdr));


                    hdr.flags_ack_ = 1; // ack the deconnection.
                    hdr.last_byte_ = endp_ctx_ptr->last_byte_received_;

                    endp_ctx_ptr->last_segment_acknoledged_ = 1;

                    if(snptp_SendToLower(ctx, hdr, NULL, 0, endp_ctx_ptr->endpoint_) < 0)
                    {
                        // fatal error.

                        pthread_mutex_unlock(&ctx->map_endpoint_mtx_);
                        errno = ENOSPC;

                        assert((((void)"Could not snptp_SendToLower."), 0));
                        return -1;
                    }
                }
            }
        }

        if(endp_ctx_ptr->ack_counter_ > 0 && endp_ctx_ptr->ack_counter_ % 200 == 0)
        {
            dbprintf("-> %d\n", endp_ctx_ptr->ack_counter_);
        }

#if defined(SNPTP_ACTIVATE_TIMEOUT)
        if(endp_ctx_ptr->ack_counter_ > 0)
        { // While last segment was sont acked, keep increasing.
            ++endp_ctx_ptr->ack_counter_;
        }
#endif

        if(SNPTP_DEFAULT_ACK_TIME_TRESHOLD <
           (endp_ctx_ptr->ack_counter_ * SNPTP_SLEEP_TIME_USEC)) // treshold of time waited for ack
        {                                                        // too much time waited. Ask for deco.
            if(endp_ctx_ptr->state_ == SNPTP_STATE_ESTABLISHED)
            {
                // deco
                dbprintf("-> We waited too long for an ack while in established state. Sending FIN flag.\n");

                snptp_SegmentHeader_t hdr;
                memset(&hdr, 0, sizeof(hdr));

                hdr.flags_fin_ = 1; // ask for deco.

                endp_ctx_ptr->state_       = SNPTP_STATE_FIN_WAIT;
                endp_ctx_ptr->ack_counter_ = 1;

                if(snptp_SendToLower(ctx, hdr, NULL, 0, endp_ctx_ptr->endpoint_) < 0)
                {
                    // fatal error.
                    pthread_mutex_unlock(&ctx->map_endpoint_mtx_);
                    errno = ENOSPC;

                    assert((((void)"Could not snptp_SendToLower."), 0));
                    return -1;
                }
            }
            else
            {
                if(endp_ctx_ptr->state_ == SNPTP_STATE_SYN_WAIT)
                { // special treatment, send a fin.
                    // send a fin
                    dbprintf(
                        "-> We waited too long for an ack while in waiting state(SNPTP_STATE_SYN_WAIT).\n"
                        "-> Sending FIN flag.\n");

                    snptp_SegmentHeader_t hdr;
                    memset(&hdr, 0, sizeof(hdr));

                    hdr.flags_fin_ = 1; // ask for deco.

                    endp_ctx_ptr->state_       = SNPTP_STATE_FIN_WAIT;
                    endp_ctx_ptr->ack_counter_ = 1;

                    if(snptp_SendToLower(ctx, hdr, NULL, 0, endp_ctx_ptr->endpoint_) < 0)
                    {
                        // fatal error.
                        pthread_mutex_unlock(&ctx->map_endpoint_mtx_);
                        errno = ENOSPC;

                        assert((((void)"Could not snptp_SendToLower."), 0));
                        return -1;
                    }
                    continue;
                }

                // we are in fin wait and the peer took too long to respond.

                map_remove(&ctx->map_endpoint_, key);
                iter.node = NULL;

                dbprintf("-> We waited too long while in a wait state. (Hard disconnect).\n");
            }
        }
    }

    pthread_mutex_unlock(&ctx->map_endpoint_mtx_);
    return 0;
}

void snptp_DeInitCtx(snptp_Context_t* ctx)
{
    if(ctx == NULL)
    {
        return;
    }

    pthread_mutex_lock(&ctx->map_endpoint_mtx_);

    map_iter_t  iter = map_iter();
    const char* key;

    while((key = map_next(&ctx->map_endpoint_, &iter)) != NULL)
    {
        snptp_EndpointCtx_t* endp;
        endp = *map_get(&ctx->map_endpoint_, key);
        snptp_DeInitEndpointCtx(endp);
        free(endp);
    }

    map_deinit(&ctx->map_endpoint_);

    pthread_mutex_unlock(&ctx->map_endpoint_mtx_); // sketchy but heyy
    pthread_mutex_destroy(&ctx->map_endpoint_mtx_);
}

int snptp_InitEndpointCtx(snptp_EndpointCtx_t* endp_ctx, const char* addr)
{
    if(endp_ctx == NULL || addr == NULL)
    {
        errno = EFAULT;
        return -1;
    }

    memset(endp_ctx, 0, sizeof(snptp_EndpointCtx_t));

    /* Segment in */
    if((endp_ctx->segment_in_ = (fifo_t)malloc(sizeof(struct fifo_descriptor))) == NULL)
    {
        snptp_DeInitEndpointCtx(endp_ctx);
        return -1;
    }

    if((endp_ctx->fifo_buffer_segment_in_ =
            (Payload8_t*)malloc(sizeof(snptp_Segment_t*) * SNPTP_SEGMENT_BUFFER_BACKLOG)) == NULL)
    {
        snptp_DeInitEndpointCtx(endp_ctx);
        return -1;
    }

    fifo_create_static(endp_ctx->segment_in_,
                       endp_ctx->fifo_buffer_segment_in_,
                       SNPTP_SEGMENT_BUFFER_BACKLOG,
                       sizeof(snptp_Segment_t*));
    /* Segment out */
    if((endp_ctx->segment_out_ = (fifo_t)malloc(sizeof(struct fifo_descriptor))) == NULL)
    {
        snptp_DeInitEndpointCtx(endp_ctx);
        return -1;
    }

    if(socket_StrToIPV4(addr, &endp_ctx->endpoint_) < 0)
    {
        snptp_DeInitEndpointCtx(endp_ctx);
        return -1;
    }

    if((endp_ctx->fifo_buffer_segment_out_ =
            (Payload8_t*)malloc(sizeof(snptp_Segment_t*) * SNPTP_SEGMENT_BUFFER_BACKLOG)) == NULL)
    {
        snptp_DeInitEndpointCtx(endp_ctx);
        return -1;
    }

    if(fifo_create_static(endp_ctx->segment_out_,
                          endp_ctx->fifo_buffer_segment_out_,
                          SNPTP_SEGMENT_BUFFER_BACKLOG,
                          sizeof(snptp_Segment_t*)) == NULL)
    {
        errno = EFAULT;
        return -1;
    }

    endp_ctx->state_ = SNPTP_STATE_CLOSED;

    endp_ctx->last_segment_acknoledged_ = 1;

    return 0;
}

void snptp_DeInitEndpointCtx(snptp_EndpointCtx_t* ctx)
{
    if(ctx == NULL)
    {
        return;
    }

    if(ctx->segment_in_)
    {
        free(ctx->segment_in_);
    }

    if(ctx->fifo_buffer_segment_in_)
    {
        free(ctx->fifo_buffer_segment_in_);
    }

    if(ctx->segment_out_)
    {
        free(ctx->segment_out_);
    }

    if(ctx->fifo_buffer_segment_out_)
    {
        free(ctx->fifo_buffer_segment_out_);
    }
}

ssize_t snptp_ReadFrom(snptp_Context_t* ctx, Payload8_t* buffer, size_t n, const char* addr)
{
    if(n == 0)
    {
        return 0;
    }

    if(ctx == NULL || buffer == NULL || addr == NULL)
    {
        return -1;
    }

    snptp_EndpointCtx_t** endp_ctx_ptr_ptr;
    snptp_EndpointCtx_t*  endp_ctx_ptr;

    pthread_mutex_lock(&ctx->map_endpoint_mtx_);

    endp_ctx_ptr_ptr = map_get(&ctx->map_endpoint_, addr);

    if(endp_ctx_ptr_ptr == NULL || (*endp_ctx_ptr_ptr)->state_ != SNPTP_STATE_ESTABLISHED)
    {
        pthread_mutex_unlock(&ctx->map_endpoint_mtx_);
        return -1;
    }

    endp_ctx_ptr = *endp_ctx_ptr_ptr;

    size_t bytes_read = 0;
    size_t to_read    = Clamp(n, endp_ctx_ptr->bytes_in_surplus_);

    memcpy(buffer, endp_ctx_ptr->surplus_, to_read);

    // shift the surplus buff
    memmove(endp_ctx_ptr->surplus_, endp_ctx_ptr->surplus_ + to_read, endp_ctx_ptr->bytes_in_surplus_ - to_read);

    endp_ctx_ptr->bytes_in_surplus_ -= to_read;
    bytes_read += to_read;

    // want to read moore
    while(bytes_read < n)
    {
        if(fifo_is_empty(endp_ctx_ptr->segment_in_))
        {
            pthread_mutex_unlock(&ctx->map_endpoint_mtx_);
            return bytes_read;
        }

        snptp_Segment_t* segment;

        if(!fifo_get(endp_ctx_ptr->segment_in_, &segment))
        {
            pthread_mutex_unlock(&ctx->map_endpoint_mtx_);
            errno = EFAULT;
            return -1;
        }

        to_read = Clamp(n - bytes_read, segment->hdr_.segment_length_ - SNPTP_HEADER_LEN);

        memcpy(buffer + bytes_read, segment->data_, to_read);

        // copy unread bytes to surplus
        memcpy(endp_ctx_ptr->surplus_,
               segment->data_ + to_read,
               segment->hdr_.segment_length_ - SNPTP_HEADER_LEN - to_read);

        free(segment);

        bytes_read += to_read;
    }

    pthread_mutex_unlock(&ctx->map_endpoint_mtx_);
    return bytes_read;
}

ssize_t snptp_WriteTo(snptp_Context_t* ctx, const Payload8_t* buffer, size_t len, const char* addr)
{
    if(len == 0)
    {
        return 0;
    }

    if(ctx == NULL || buffer == NULL || addr == NULL)
    {
        errno = EFAULT;
        return -1;
    }

    snptp_EndpointCtx_t** endp_ctx_ptr_ptr;
    snptp_EndpointCtx_t*  endp_ctx_ptr;

    pthread_mutex_lock(&ctx->map_endpoint_mtx_);

    endp_ctx_ptr_ptr = map_get(&ctx->map_endpoint_, addr);

    if(endp_ctx_ptr_ptr == NULL || (*endp_ctx_ptr_ptr)->state_ != SNPTP_STATE_ESTABLISHED)
    {

        pthread_mutex_unlock(&ctx->map_endpoint_mtx_);
        errno = EPERM;
        return -1;
    }

    endp_ctx_ptr = *endp_ctx_ptr_ptr;

    if(fifo_is_full(endp_ctx_ptr->segment_out_))
    {
        pthread_mutex_unlock(&ctx->map_endpoint_mtx_);
        errno = EAGAIN;
        return 0;
    }

    snptp_Segment_t* snptp_segment;

    len = Clamp(len, SNPTP_MAX_PAYLOAD_LEN);

    if((snptp_segment = (snptp_Segment_t*)malloc(SNPTP_HEADER_LEN + len)) == NULL)
    {
        pthread_mutex_unlock(&ctx->map_endpoint_mtx_);
        return -1;
    }

    // zeros the header
    memset(snptp_segment, 0, sizeof(snptp_SegmentHeader_t));

    //  data to payload section
    memcpy(((Payload8_t*)snptp_segment) + SNPTP_HEADER_LEN, buffer, len);

    snptp_segment->hdr_.segment_length_ = SNPTP_HEADER_LEN + len;

    if(!fifo_add(endp_ctx_ptr->segment_out_, &snptp_segment))
    {
        errno = ENOSPC;
        pthread_mutex_unlock(&ctx->map_endpoint_mtx_);
        return -1;
    }

    pthread_mutex_unlock(&ctx->map_endpoint_mtx_);
    return len;
}

int snptp_Close(snptp_Context_t* ctx, const char* addr)
{
    if(ctx == NULL || addr == NULL)
    {
        errno = EFAULT;
        return -1;
    }

    pthread_mutex_lock(&ctx->map_endpoint_mtx_);

    snptp_EndpointCtx_t** endp_ctx_ptr_ptr = map_get(&ctx->map_endpoint_, addr);

    if(endp_ctx_ptr_ptr == NULL) // check if the map contains a value for a key
    {
        pthread_mutex_unlock(&ctx->map_endpoint_mtx_);
        assert((((void)"Could not close an non open connection."), 0));
        return -1;
    }

    snptp_EndpointCtx_t* endp_ctx_ptr = *endp_ctx_ptr_ptr;

    snptp_SegmentHeader_t hdr;
    memset(&hdr, 0, sizeof(hdr));

    hdr.flags_fin_ = 1; // ask for deco.

    endp_ctx_ptr->state_       = SNPTP_STATE_FIN_WAIT;
    endp_ctx_ptr->ack_counter_ = 1;

    if(snptp_SendToLower(ctx, hdr, NULL, 0, endp_ctx_ptr->endpoint_) < 0)
    {
        // fatal error.
        pthread_mutex_unlock(&ctx->map_endpoint_mtx_);
        assert((((void)"Could not snptp_SendToLower."), 0));
        return -1;
    }

    pthread_mutex_unlock(&ctx->map_endpoint_mtx_);

    return 0;
}

int snptp_Connect(snptp_Context_t* ctx, const char* addr)
{
    if(ctx == NULL || addr == NULL)
    {
        errno = EFAULT;
        return -1;
    }

    pthread_mutex_lock(&ctx->map_endpoint_mtx_);

    if(map_get(&ctx->map_endpoint_, addr) != NULL)
    {
        pthread_mutex_unlock(&ctx->map_endpoint_mtx_);

        errno = EFAULT;
        return -1; // exist already.
    }

    snptp_EndpointCtx_t* new_endp;

    if((new_endp = (snptp_EndpointCtx_t*)malloc(sizeof(*new_endp))) == NULL)
    {
        // fatal error.
        pthread_mutex_unlock(&ctx->map_endpoint_mtx_);
        assert((((void)"Could not malloc new_endp."), 0));
        return -1;
    }

    if(snptp_InitEndpointCtx(new_endp, addr) < 0)
    {
        // fatal error.
        free(new_endp);

        pthread_mutex_unlock(&ctx->map_endpoint_mtx_);
        assert((((void)"Could not malloc snptp_InitEndpointCtx."), 0));
        return -1;
    }

    if(map_set(&ctx->map_endpoint_, addr, new_endp) < 0)
    {
        errno = EPERM;
        // fatal error.
        free(new_endp);
        pthread_mutex_unlock(&ctx->map_endpoint_mtx_);
        assert((((void)"Could not map_set."), 0));
        return -1;
    }

    new_endp->state_       = SNPTP_STATE_SYN_WAIT;
    new_endp->ack_counter_ = 1;

    snptp_SegmentHeader_t hdr;
    memset(&hdr, 0, sizeof(hdr));

    hdr.flags_syn_ = 1; // ask for co.

    if(snptp_SendToLower(ctx, hdr, NULL, 0, new_endp->endpoint_) < 0)
    {
        // fatal error.
        pthread_mutex_unlock(&ctx->map_endpoint_mtx_);
        assert((((void)"Could not snptp_SendToLower."), 0));
        return -1;
    }

    pthread_mutex_unlock(&ctx->map_endpoint_mtx_);

    return 0;
}

/*
char* snptp_EndpointStrGetNodeName(const socket_Endpoint_t* addr)
{
    static char str[SOCKET_IPV6_ADDR_STR_LEN];

    socket_EndpointStr_t* endp_str;
    if((endp_str = socket_EndpointToStr(addr)) == NULL)
    {
        return NULL;
    }

    memcpy(endp_str->node_name, str, strlen(endp_str->node_name));

    socket_DeInitEndpointStr(endp_str);

    return str;
}
 */
