/**
 * @file crc32.h
 * @author All credit to Mark Adler
 * @date 1995-2006, 2010
 *
 * @copyright Copyright (C) 1995-2006, 2010 Mark Adler
 *
 */

#ifndef SNTPRP_CRC32_H
#define SNTPRP_CRC32_H

#include <stddef.h>
#include <stdint.h>

/**
 * @brief Generate tables for a byte-wise 32-bit CRC calculation on the polynomial:
 * x^32+x^26+x^23+x^22+x^16+x^12+x^11+x^10+x^8+x^7+x^5+x^4+x^2+x+1.
 * See: https://en.wikipedia.org/wiki/Cyclic_redundancy_check at CRC32
 * It's used by ISO 3309 (HDLC), ANSI X3.66 (ADCCP), FIPS PUB 71, FED-STD-1003,
 * ITU-T V.42, ISO/IEC/IEEE 802-3 (Ethernet), SATA, MPEG-2, PKZIP, Gzip, Bzip2,
 * POSIX cksum,[52] PNG,[53] ZMODEM,
 * https://tools.ietf.org/html/rfc1952#section-8
 *
 * Polynomials over GF(2) are represented in binary, one bit per coefficient,
 * with the lowest powers in the most significant bit.  Then adding polynomials
 * is just exclusive-or, and multiplying a polynomial by x is a right shift by
 * one.  If we call the above polynomial p, and represent a byte as the
 * polynomial q, also with the lowest power in the most significant bit (so the
 * byte 0xb1 is the polynomial x^7+x^3+x+1), then the CRC is (q*x^32) mod p,
 * where a mod b means the remainder after dividing a by b.
 *
 * This calculation is done using the shift-register method of multiplying and
 * taking the remainder.  The register is initialized to zero, and for each
 * incoming bit, x^32 is added mod p to the register if the bit is a one (where
 * x^32 mod p is p+x^32 = x^26+...+1), and the register is multiplied mod p by
 * x (which is shifting right by one and adding x^32 mod p if the bit shifted
 * out is a one).  We start with the highest power (least significant bit) of
 * q and repeat for all eight bits of q.
 *
 * The first table is simply the CRC of all possible eight bit values.  This is
 * all the information needed to generate CRCs on data a byte at a time for all
 * combinations of CRC register values and incoming bytes.  The remaining tables
 * allow for word-at-a-time CRC calculation for both big-endian and little-
 * endian machines, where a word is four bytes.
 *
 * @param crc Base value of the crc
 * @param buf Data
 * @param len Len of data
 * @return uint32_t computed crc32 of buf
 */
uint32_t snptrp_CRC32Compute(uint32_t crc, const uint8_t* buf, size_t len);

#endif
