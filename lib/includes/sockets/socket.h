/**
 * @file socket.h
 * @author Etienne Malaboeuf
 * @brief 
 * @version 0.1
 * 
    Simple Non Pragmatic Token Ring Protocol
    Copyright (C) 2020  Etienne Malaboeuf

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

 * 
 */

#ifndef IMPLEMENTATION_SOCKET_H
#define IMPLEMENTATION_SOCKET_H

/* #define _GNU_SOURCE */
/* #define _POSIX_C_SOURCE 200112L */

#include <netdb.h>
#include <stdint.h>

/* TYPES */

/**
 * @brief Voir socket_type.h
 *
 */
typedef enum
{
    ENUM_SOCKET_TYPE_DATAGRAM = SOCK_DGRAM,
    ENUM_SOCKET_TYPE_STREAM   = SOCK_STREAM
    // ENUM_SOCKET_TYPE_RAW = SOCK_RAW
} socket_SockType_e;

/**
 * @brief voir socket.h
 *       AF_UNIX, AF_LOCAL   Communication locale              unix(7)
 *       AF_INET             Protocoles Internet IPv4          ip(7)
 *       AF_INET6            Protocoles Internet IPv6          ipv6(7)
 *       AF_IPX              IPX - Protocoles Novell
 *       AF_NETLINK          Interface utilisateur noyau       netlink(7)
 *       AF_X25              Protocole ITU-T X.25 / ISO-8208   x25(7)
 *       AF_AX25             Protocole AX.25 radio amateur
 *       AF_ATMPVC           Accès direct ATM PVCs
 *       AF_APPLETALK        Appletalk                         ddp(7)
 *       AF_PACKET           Interface paquet bas-niveau       packet(7)
 *
 */
typedef enum
{
    ENUM_SOCKET_FAMILY_IPV4   = AF_INET,
    ENUM_SOCKET_FAMILY_IPV6   = AF_INET6,
    ENUM_SOCKET_FAMILY_UNSPEC = AF_UNSPEC
} socket_AddressFamily_e;

/**
 * @brief voir netdb.H
 *  define AI_PASSIVE	0x0001	 Socket address is intended for `bind'. Ignored
 *  if node_name given to create socket.
 *  define AI_CANONNAME	0x0002	 Request for canonical name.
 *  define AI_NUMERICHOST	0x0004	 Don't use name resolution.
 *  define AI_V4MAPPED	0x0008	 IPv4 mapped addresses are acceptable.
 *  define AI_ALL		0x0010	 Return IPv4 mapped and IPv6 addresses.
 *  define AI_ADDRCONFIG	0x0020	Use configuration of this host to choose
 *  returned address type.
 */
typedef enum
{
    ENUM_SOCKET_RESOLVER_FLAGS_PASSIVE = AI_PASSIVE
} socket_SockFlags_e;
/**
 * @brief
 * MSG_OOB	Receive Out of Band data. This is how to get data that has been sent to you with the MSG_OOB flag in send().
 * As the receiving side, you will have had signal SIGURG raised telling you there is urgent data. In your handler for
 * that signal, you could call recv() with this MSG_OOB flag.
 * MSG_PEEK	If you want to call recv() “just for pretend”,
 * you can call it with this flag. This will tell you what’s waiting in the buffer for when you call recv() “for real”
 * (i.e. without the MSG_PEEK flag. It’s like a sneak preview into the next recv() call.
 * MSG_WAITALL	Tell recv() to
 * not return until all the data you specified in the len parameter. It will ignore your wishes in extreme
 * circumstances, however, like if a signal interrupts the call or if some error occurs or if the remote side closes the
 * connection, etc. Don’t be mad with it.
 * MSG_DONTROUTE	Don’t send this data over a router, just keep it local.
 * MSG_DONTWAIT	If send() would block because outbound traffic is clogged, have it return EAGAIN. This is like a “enable
 * non-blocking just for this send.” See the section on blocking for more details.
 * MSG_NOSIGNAL	If you send() to a
 * remote host which is no longer recv()ing, you’ll typically get the signal SIGPIPE. Adding this flag prevents that
 * signal from being raised.
 */
typedef enum
{
    ENUM_SOCKET_IO_FLAGS_OOB       = MSG_OOB,
    ENUM_SOCKET_IO_FLAGS_PEEK      = MSG_PEEK,
    ENUM_SOCKET_IO_FLAGS_WAITALL   = MSG_WAITALL,
    ENUM_SOCKET_IO_FLAGS_DONTROUTE = MSG_DONTROUTE,
    ENUM_SOCKET_IO_FLAGS_DONTWAIT  = MSG_DONTWAIT,
    ENUM_SOCKET_IO_FLAGS_NOSIGNAL  = MSG_NOSIGNAL,
} socket_SockIOFlags_e;

typedef int32_t socket_SocketFD_t;

/* ENDPOINT and CONVERSION*/

#define SOCKET_IPV6_ADDR_STR_LEN INET6_ADDRSTRLEN
#define SOCKET_IPV4_ADDR_STR_LEN INET_ADDRSTRLEN
#define SOCKET_INET_SERVICELEN 7

typedef struct sockaddr_storage socket_Endpoint_t;

typedef struct
{
    socket_AddressFamily_e family;
    char*                  node_name;
    char*                  service;
} socket_Query_t;

typedef socket_Query_t socket_EndpointStr_t;

socket_EndpointStr_t* socket_EndpointToStr(const socket_Endpoint_t* addr);
int                   socket_GetDestEndpoint(socket_SocketFD_t sock_fd, socket_Endpoint_t* endp);
void                  socket_FreeEndpointStr(socket_EndpointStr_t* endp);

int socket_IPV4ToStr(uint32_t addr, char* out);
int socket_StrToIPV4(const char* addr, uint32_t* out);

/* RESOLVERS */

typedef struct addrinfo socket_ResolverEndpointInfo_t__;

typedef struct
{
    socket_ResolverEndpointInfo_t__* first_;
    socket_ResolverEndpointInfo_t__* it_;
} socket_Resolver_t;

socket_Resolver_t* socket_CreateResolver(socket_Query_t query, socket_SockType_e type, socket_SockFlags_e flags);
socket_Resolver_t* socket_ResolverNext(socket_Resolver_t* it);
socket_Endpoint_t* socket_ResolverGetEndpoint(const socket_Resolver_t* it);
void               socket_FreeResolver(socket_Resolver_t* resolver);

/* Should ne be used to setup sockets, for presentation only */
/* void socket_AddrStrPresentationToEndpoint(); */
/* Should ne be used to setup sockets, for presentation only */
/* void socket_EndpointToStrPresentation(); */
#endif
