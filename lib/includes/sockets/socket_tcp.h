/**
 * @file socket_tcp.h
 * @author Etienne Malaboeuf
 * @brief 
 * @version 0.1
 * 
    Simple Non Pragmatic Token Ring Protocol
    Copyright (C) 2020  Etienne Malaboeuf

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

 * 
 */

#ifndef IMPLEMENTATION_SOCKET_TCP_H
#define IMPLEMENTATION_SOCKET_TCP_H

#include <stdbool.h>

#include "socket.h"

/* RESOLVERS */

typedef socket_Resolver_t socket_TCPResolver_t;

socket_TCPResolver_t* socket_CreateTCPResolver(socket_Query_t query);
socket_TCPResolver_t* socket_TCPResolverNext(socket_TCPResolver_t* it);
socket_Endpoint_t*    socket_TCPResolverGetEndpoint(const socket_TCPResolver_t* it);
void                  socket_DeInitTCPResolver(socket_TCPResolver_t* resolver);

/* TCPSOCKET */

/* typedef struct
{
    socket_SocketFD_t socket_fd_;
    //
} socket_TCPSocket_t; */

typedef socket_SocketFD_t socket_TCPSocket_t;

socket_TCPSocket_t socket_CreateTCPSocket(const socket_TCPResolver_t* it);
int                socket_TCPGetDestEndpoint(socket_TCPSocket_t sock, socket_Endpoint_t* endp);
int                socket_CloseTCPSocket(socket_TCPSocket_t sock);

int socket_TCPReceive(socket_TCPSocket_t sock, void* buf, size_t count, size_t* bytes_read, socket_SockIOFlags_e flags);
int socket_TCPSend(socket_TCPSocket_t   sock,
                   const void*          buf,
                   size_t               len,
                   size_t*              bytes_written,
                   socket_SockIOFlags_e flags);

ssize_t socket_TCPReceiveSome(socket_TCPSocket_t sock, void* buf, size_t count, socket_SockIOFlags_e flags);
ssize_t socket_TCPSendSome(socket_TCPSocket_t sock, const void* buf, size_t len, socket_SockIOFlags_e flags);

/* ACCEPTORS */

/* typedef struct
{
    socket_SocketFD_t socket_fd_;
    socket_Endpoint_t local_endp;
} socket_TCPAcceptor_t; */

typedef socket_SocketFD_t socket_TCPAcceptor_t;

socket_TCPAcceptor_t socket_CreateTCPAcceptor(const socket_TCPResolver_t* it, int backlog, bool reuse_addr);
socket_TCPSocket_t   socket_TCPAccept(socket_TCPAcceptor_t acceptor, socket_Endpoint_t* from);
int                  socket_CloseTCPAcceptor(socket_TCPAcceptor_t acceptor);

#endif
