/**
 * @file socket_udp.h
 * @author Etienne Malaboeuf
 * @brief 
 * @version 0.1
 * 
    Simple Non Pragmatic Token Ring Protocol
    Copyright (C) 2020  Etienne Malaboeuf

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

 * 
 */
#ifndef IMPLEMENTATION_SOCKET_UDP_H
#define IMPLEMENTATION_SOCKET_UDP_H

#include <stdbool.h>

#include "socket.h"

/* RESOLVERS */

typedef socket_Resolver_t socket_UDPResolver_t;

socket_UDPResolver_t* socket_CreateUDPResolver(socket_Query_t query);
socket_UDPResolver_t* socket_UDPResolverNext(socket_UDPResolver_t* it);
socket_Endpoint_t*    socket_UDPResolverGetEndpoint(const socket_UDPResolver_t* it);
void                  socket_FreeUDPResolver(socket_UDPResolver_t* resolver);

/* UDPSOCKET */

/* typedef struct
{
    socket_SocketFD_t socket_fd_;
      bool connected_;
// socket_Endpoint_t local_endp;
}
socket_UDPSocket_t;
*/

typedef socket_SocketFD_t socket_UDPSocket_t;

// bind, dont connect
socket_UDPSocket_t socket_CreateUDPSocket(const socket_UDPResolver_t* it, bool reuse_addr);
socket_UDPSocket_t socket_CreateUDPConnectedSocket(const socket_UDPResolver_t* it);
int                socket_UDPGetDestEndpoint(socket_UDPSocket_t sock, socket_Endpoint_t* endp);
// connect to dist, it is possible to change the connected endp as you want (given the same addr family)
// use null endp to unconnect
int socket_UDPConnect(socket_UDPSocket_t sock, const socket_UDPResolver_t* it);
int socket_UDPConnectEndpoint(socket_UDPSocket_t sock, const socket_Endpoint_t* endp);
int socket_CloseUDPSocket(socket_UDPSocket_t sock);

// for connected sockets
int socket_UDPReceive(socket_UDPSocket_t sock, void* buf, size_t count, size_t* bytes_read, socket_SockIOFlags_e flags);
int socket_UDPSend(socket_UDPSocket_t   sock,
                   const void*          buf,
                   size_t               len,
                   size_t*              bytes_written,
                   socket_SockIOFlags_e flags);

ssize_t socket_UDPReceiveSome(socket_UDPSocket_t sock, void* buf, size_t count, socket_SockIOFlags_e flags);
ssize_t socket_UDPSendSome(socket_UDPSocket_t sock, const void* buf, size_t len, socket_SockIOFlags_e flags);

ssize_t socket_UDPReceiveSomeFrom(socket_UDPSocket_t   sock,
                                  void*                buf,
                                  size_t               count,
                                  socket_SockIOFlags_e flags,
                                  socket_Endpoint_t*   from);
ssize_t socket_UDPSendSomeTo(socket_UDPSocket_t       sock,
                             const void*              buf,
                             size_t                   len,
                             socket_SockIOFlags_e     flags,
                             const socket_Endpoint_t* to);

#endif
