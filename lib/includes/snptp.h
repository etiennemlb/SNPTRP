/**
 * @file snptp.h
 * @author Etienne Malaboeuf
 * @brief
 * @version 0.1
 *
    Simple Non Pragmatic Token Ring Protocol
    Copyright (C) 2020  Etienne Malaboeuf

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

 *
 */

#ifndef SNTPRP_SNPTP_H
#define SNTPRP_SNPTP_H

#include <pthread.h>
/* #ifndef __STDC_NO_ATOMICS__
#include <stdatomic.h>
#endif */

#include <sockets/socket.h>

#include "fifo.h"
#include "map.h"
#include "snptrp.h"
#include "types.h"

/**
 * @brief This is the default backlog allowed in the writing and receiving buffers. (segment_in_ and segment_out_.)
 *
 */
#define SNPTP_SEGMENT_BUFFER_BACKLOG 32

/**
 * @brief Time in µseconds recommended for the process loop.
 *
 */
#define SNPTP_SLEEP_TIME_USEC 1500

/**
 * @brief Given a time length in seconds, converts it to µsecondes.
 *
 */
#define SNPTP_ACK_TIME_TRESHOLD(sec) (sec * 1000000)

/**
 * @brief Default wait time for an acknoledgment. The peer might send an fin flag past this threshold.
 *
 */
#define SNPTP_DEFAULT_ACK_TIME_TRESHOLD SNPTP_ACK_TIME_TRESHOLD(2)

/**
 * @brief activate the mecanisme sending fin flag if a time treshold is reached with waiting for an ack.
 *
 */
#define SNPTP_ACTIVATE_TIMEOUT

/**
 * @brief Thus enumeration represents the different states of a snptp connection.
 *
 */
typedef enum
{
    SNPTP_STATE_CLOSED, /**< The closed state, general, a endpoint is not stored in memory of the this state would have
                           been set. */
    SNPTP_STATE_SYN_WAIT, /**< The synwait state is needed when a peer asked for a connection to be created but no ask
                             has been received.*/
    SNPTP_STATE_ESTABLISHED, /**< The established state, for when a peer has established connection with an other peer.
                              */
    SNPTP_STATE_FIN_WAIT     /**< As for the synwait, this falg is for when a peer is waiting for an ack. */
} snptp_ConnectionState_t;

/**
 * @brief This structure represents the SNPTP segment header as it comes from the wire (or the lower layers).
 *
 */
typedef struct
{
    Size16_t segment_length_; /**< The total length of the segment, header included. */

    Flag8_t
        flags_ack_ : 1; /**< 0x01, the flag tels the receiver that the segment is to acknoledge the last recieved
                           segment. The last byte should be filled if the sending peer is in the established state. */
    Flag8_t flags_ask_resend_ : 1; /**< 0x02, The peer is asking for the destination to resend the last segment. If
                                      there is no last segment (still not established, or just barely), the destination
                                      might ask to be disconnected. */
    Flag8_t flags_syn_ : 1;        /**< 0x04, Ask for a new connection to be established. */
    Flag8_t flags_fin_ : 1;        /**< 0x08, Ask for a deconnection. */
    Flag8_t flags_unused___ : 4;

    Payload8_t last_byte_; /** This a kind of proof that the acknoledgment is sincere. This is the last byte of the last
                              segment received. */
    CheckSum32_t check_sum_; /* The checksum is a crc32 as defined in the followinf RFC:
                                https://tools.ietf.org/html/rfc1952#section-8 . */
} snptp_SegmentHeader_t;

/**
 * @brief Use this to get the len of the SNPTP header.
 *
 */
#define SNPTP_HEADER_LEN (sizeof(snptp_SegmentHeader_t))
/**
 * @brief This is the hard coded max length of a snptp segment (without it's header).
 *
 */
#define SNPTP_MAX_PAYLOAD_LEN (1200)

/**
 * @brief This represents the SNPP segment. DO NOT use sizeof in the structure as it is not fixed size. In fact,
 * according the length pf the payload, just enough memory is allocated. You might want to use the field in the header
 * named segment_length_ to get the length. */
typedef struct
{
    snptp_SegmentHeader_t hdr_;
    Payload8_t            data_[SNPTP_MAX_PAYLOAD_LEN];
} snptp_Segment_t;

/**
 * @brief This represents a endpoint.
 *
 */
typedef struct
{
    fifo_t segment_in_; /**< This is used to read the data that has been received. It's used by the snptp_ReadFrom
                           function. */
    Payload8_t* fifo_buffer_segment_in_;

    uint32_t        have_last_segment_; /**< if at least one segment with payload as been sent. */
    snptp_Segment_t last_sent_segment_; /**< if have_last_segment is set, this is too. */
    uint32_t        ack_counter_; /**< increase as the user want to send data but the distant peer dont answer. After a
                                     treshold, ask for deco. */

    Payload8_t surplus_[SNPTP_MAX_PAYLOAD_LEN]; /**< Used to not throw away usefull payload. */
    size_t     bytes_in_surplus_;

    fifo_t      segment_out_; /**< This is used when the user writes using the snptp_WriteTo function. */
    Payload8_t* fifo_buffer_segment_out_;

    // :mapAttenteenvoiAcquittement
    Payload8_t last_byte_sent_;     /**< same as looking at the end of the last_segment_. */
    Payload8_t last_byte_received_; /**< used for acknowledgement of segment. */
    int32_t
        last_segment_acknoledged_; /**< If this is set, the local peer is alowed to send more data to the endpoint. */

    snptp_ConnectionState_t state_; /**< The current state of the peer regarding the connection with the endoint. */
    /* uint32_t                ack_loop_cpt_; // count the number of loops  */

    Address32_t endpoint_; //!< not currently used
} snptp_EndpointCtx_t;

typedef map_t(snptp_EndpointCtx_t*) snptp_MapEndpoint_t;

/**
 * @brief This structure represent the snptp context. It contains all the datastructures needed to interpret snptp
 * segment.
 *
 */
typedef struct
{
    snptp_MapEndpoint_t map_endpoint_;     /**< Maps strings(representing addr) to an endpoint_ctx_ptr. */
    pthread_mutex_t     map_endpoint_mtx_; /**< The user may right/read/etc.. while we process an entry. */


    fifo_t global_in_buffer_snptrp_; /**< contains structures (not ptr) of type snptrp_SegmentHolder_tThis is used to
                                        communicate with the lower layers. */
    pthread_mutex_t* global_in_mtx_;

    fifo_t global_out_buffer_snptrp_; /**< contains structures (not ptr) of type snptrp_SegmentHolder_t. This is used to
                                         communicate with the lower layers. */
    pthread_mutex_t* global_out_mtx_;

} snptp_Context_t;

/**
 * @brief This is the first function that shoul be called.
 *
 * @param ctx A valid ptr to a snptp ctx.
 * @param snptrp_ctx A valid ptr to the a snptrp ctx.
 * @return int 0 if all good, -1 if error. Errno set appropriately.
 */
int snptp_InitCtx(snptp_Context_t* ctx, snptrp_Context_t* snptrp_ctx);

/**
 * @brief This function needs to be called in order to process the incoming segments. It's given to the user to call it.
 *
 * @param ctx
 * @return int 0 if all good, -1 if error. Errno set appropriately.
 */
int snptp_ProtoProcessLoop(snptp_Context_t* ctx);

/**
 * @brief Use this function do deinit the endpoint ctx. Only use this function of protoprocessloop is not beeing called.
 * does not free the ctx.
 *
 * @param ctx
 */
void snptp_DeInitCtx(snptp_Context_t* ctx);

/**
 * @brief Create a new endpoint and adds it to the ctx. The state of the constructed endpoint is closed.
 *
 * @param ctx
 * @param addr The endpoint as a char array (cstring like).
 * @return int 0 if all good, -1 if error. Errno set appropriately.
 */
int snptp_InitEndpointCtx(snptp_EndpointCtx_t* ctx, const char* addr);

/**
 * @brief Use this function do deinit the endpoint ctx. Only use this function of protoprocessloop is not beeing called.
 *
 * @param ctx A valid, functionnal ctx.
 */
void snptp_DeInitEndpointCtx(snptp_EndpointCtx_t* ctx);


/**
 * @brief Use this functino to receive data from a connected enpoint. This function is non blocking. Does not free the
 * ctx.
 *
 * @param ctx A valid ctx.
 * @param buffer The buffer receiving the payload.
 * @param len The len of the buffer.
 * @param addr The endpoint to read from.
 * @return ssize_t The amount of bytes read. -1 if error, errno set appropriately.
 */
ssize_t snptp_ReadFrom(snptp_Context_t* ctx, Payload8_t* buffer, size_t n, const char* addr);

/**
 * @brief Use this functino to send data to a connected enpoint. This function is non blocking.
 *
 * @param ctx A valid ctx.
 * @param buffer The buffer containing the payload.
 * @param len The len of the buffer.
 * @param addr The endpoint to write to.
 * @return ssize_t The amount of bytes written. -1 if error, errno set appropriately.
 */
ssize_t snptp_WriteTo(snptp_Context_t* ctx, const Payload8_t* buffer, size_t len, const char* addr);

/**
 * @brief Use this function to close the connect to an endpoint. If a connection is already closed (or is closing), it
 * will fail. This function is non blocking and does not tell the user if the connection has been sucessfuly closed.
 *
 * @param ctx A valid snptp ctx.
 * @param addr The endpoint.
 * @return int 0 if all good, -1 if error. Errno set appropriately.
 */
int snptp_Close(snptp_Context_t* ctx, const char* addr);

/**
 * @brief Use this function to connect to an endpoint. If a connection is already set up (or is beeing set  up), it will
 * fail. This function is non blocking and does not tell the user if the connection has been sucessfuly setup.
 *
 * @param ctx A valid snptp ctx.
 * @param addr the endpoint.
 * @return int 0 if all good, -1 if error. Errno set appropriately.
 */
int snptp_Connect(snptp_Context_t* ctx, const char* addr);

/* char* snptp_EndpointStrGetNodeName(const socket_Endpoint_t* addr); */

#endif
