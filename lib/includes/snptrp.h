/**
 * @file snptrp.h
 * @author Etienne Malaboeuf
 * @brief
 * @version 0.1
 *
    Simple Non Pragmatic Token Ring Protocol
    Copyright (C) 2020  Etienne Malaboeuf

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

 *
 */

#ifndef SNTPRP_SNPTRP_H
#define SNTPRP_SNPTRP_H

#include <pthread.h>
#include <sockets/socket.h>
#include <sockets/socket_udp.h>
#include <stdbool.h>

#include "bit_packing.h"
#include "fifo.h"
#include "types.h"

/**
 * @brief Backlog of segment to and from the snptrp layer.(to communicate with high level layers).
 *
 */
#define SNPTRP_GLOBAL_SEGMENT_BUFFER_BACKLOG 128

/**
 * @brief Recommanded time length to wait after running the proto process loop.
 *
 */
#define SNPTRP_SLEEP_TIME_USEC 5000

/**
 * @brief This represents a small header as dexcribed in the Simple Non Pragmatic Token Ring Protocol_v2.pdf file in the
 * doc folder.
 *
 */
typedef struct
{
    Flag8_t flags_2_;

    Proto8_t protocol_; /**< The current protocol that we should redirect this segment to. */

    Size16_t payload_lenght_; /**< The length of the payload (small header length not included). */

    Address32_t from_; /**< From this endpoint. */
    Address32_t to_;   /**< To an endpoint. */
} snptrp_SmallHeader_t;

#define SNPTRP_SMALLHEADER_LEN            (sizeof(snptrp_SmallHeader_t))
#define SNPTRP_BROADCAST_ADDR             (0xFFFFFFFF)
#define SNPTRP_MAX_OWNED_SEGMENT_ON_FRAME (2)
// Max len of a snptp segment
#define SNPTRP_MAX_PAYLOAD_UNIT_LEN (1208)

/**
 * @brief Represents the SNPTRP main header.
 *
 */
PACKED_STRUCT(typedef struct {
    /**
     * @brief the preamble is always the following sequence of bit:
     *        0xF00D .
     */
    Preamble_t preamble_;

    char __padding[1];

    /**
     * @brief Unused
     *
     */
    Flag8_t flags_1_;

    /**
     * @brief Represents the total length of the frame in bytes (headers included). The maximum is 2^32 but a limit as
     * been specified to prevent attacks (maybe?).
     */
    Size32_t frame_length_;

    /**
     * @brief Represents the number of small header.
     */
    Size32_t header_count_;
})
snptrp_FrameHeader_t;

#define SNPTRP_FRAME_PREAMBLE (0xF00D)

/**
 * @brief In theory, this should be 2^32 but for security reason, we (Ok, I'm) gonna limit this.
 *
 */
#define SNPTRP_MAX_FRAME_LEN (1 << 20)

#define SNPTRP_HEADER_LEN (sizeof(snptrp_FrameHeader_t))

/**
 * @brief Defines the id of the snptrp process.
 *
 */
#define SNPTRP_PROTO_SNPTP 1
// ADD MORE
#define SNPTRP_PROTO_UNKNOWN 0xFF

/**
 * @brief This structure represents the frame of the SNPTRP protocol.
 *
 */
typedef struct
{
    snptrp_FrameHeader_t hdr_;

    Payload8_t payload_;
} snptrp_Frame_t;

/**
 * @brief This structure is user to communicate segments to upper layers and for upper layers to cummunicate with
 * SNPTRP.
 *
 */
typedef struct
{
    size_t      size_;    //!< segment len.
    void*       segment_; //!< Segment data and header.
    Address32_t addr_; /**< this addr in used to either tell the upper layers where the segment comes from, or for the
                          upper layers to tell the lower layers where it should be sent. */
    // uint8_t proto_;
} snptrp_SegmentHolder_t;

/**
 * @brief This structure represent the snptrp context. It contains all the datastructures needed to interpret snptrp
 * frames.
 * 
 */
typedef struct
{
    // socket_Endpoint_t  local_addr_;             //!< local addr, used to bind the listening socket.
    socket_UDPSocket_t listening_sock_;            //!< we receive frames from the prev addr on this socket.
    Address32_t        listening_sock_as_network_; //!< used to check if the frame is aimed at us.

    // socket_Endpoint_t  next_addr_; // we send to the addr. Not used in theory as the socket is connected.
    socket_UDPSocket_t next_sock_; //!< This is the socket representing the next peer of the ring.

    // socket_Endpoint_t prev_addr_; // not used in theory as the listening socket is
    // connected to the prev addr by default.

    uint8_t original_; /**< If the peer is the original peer sending the original frame, this is set to true. This could
                          later be user to restart the ring if an error occured. */

    Payload8_t* frame_buff_in_;
    Payload8_t* frame_buff_hdr_out_;
    Payload8_t* frame_buff_payload_out_;

    fifo_t          fifo_to_upper_layers_snptp_; //!< This is used to communicate with the upper layers.
    Payload8_t*     fifo_buffer_to_upper_layers_;
    pthread_mutex_t mtx_to_upper_layers_snptp_; //!< Mutex to order the access of snptrp reveiving from upper layers.

    fifo_t          fifo_from_upper_layers_; //!< This is used to communicate with the upper layers.
    Payload8_t*     fifo_buffer_from_upper_layers_;
    pthread_mutex_t mtx_from_upper_layers_; //!< Mutex to order the access of the upper layers reveiving from snptrp.
} snptrp_Context_t;

/**
 * @brief Use this fnction to initialize a sptrp ctx.
 *
 * @param ctx
 * @param local
 * @param dest
 * @param from
 * @param port_listen
 * @param port_send
 * @return int 0 if all good, -1 if error. Errno set appropriately.
 */
int snptrp_InitCtx(snptrp_Context_t* ctx, char* local, char* dest, char* from, char* port_listen, char* port_send);

/**
 * @brief This function needs to be called in order to process the incoming frames. It's given to the user to call it.
 *
 * @param ctx
 * @return int 0 if all good, -1 if error. Errno set appropriately.
 */
int snptrp_ProtoProcessLoop(snptrp_Context_t* ctx);

/**
 * @brief Deinit a snptrp ctx. Doe not free ctx.
 *
 * @param ctx
 */
void snptrp_DeInitCtx(snptrp_Context_t* ctx);

/**
 * @brief Set the peer to sent to original frame on the ring.
 *
 * @param ctx
 * @return int 0 if all good, -1 if error. Errno set appropriately.
 */
int snptrp_SetOriginal(snptrp_Context_t* ctx);

/**
 * @brief Free the segment inside the holder. Does not free holder.
 *
 * @param holder
 */
void snptrp_DeInitSegmentHolder(snptrp_SegmentHolder_t* holder);

#endif
