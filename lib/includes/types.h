/**
 * @file types.h
 * @author Etienne Malaboeuf
 * @brief 
 * @version 0.1
 * 
    Simple Non Pragmatic Token Ring Protocol
    Copyright (C) 2020  Etienne Malaboeuf

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

 * 
 */

#ifndef SNTPRP_TYPES_H
#define SNTPRP_TYPES_H

#include <stdint.h>

/**
 * @brief This type represents a ipv4, which is also the type of address we use in SNPTRP.
 * 
 */
typedef uint32_t Address32_t;

/**
 * @brief The preamble.
 * 
 */
typedef uint16_t Preamble_t;

/**
 * @brief Represents the flags used in SNPTRP and SNPTP.
 * 
 */
typedef uint8_t Flag8_t;

typedef uint16_t Size16_t;
typedef uint32_t Size32_t;

/**
 * @brief Represents the protocole contained inside the payload of the SNPTRP frame.
 * 
 */
typedef uint8_t Proto8_t;

/**
 * @brief Any kind of payload.
 * 
 */
typedef uint8_t Payload8_t;

/**
 * @brief Represents the checksum of a snptp segment.
 * 
 */
typedef uint32_t CheckSum32_t;

#endif
