/**
 * @file main.c
 * @author Etienne Malaboeuf
 * @brief 
 * @version 0.1
 * 
    Simple Non Pragmatic Token Ring Protocol
    Copyright (C) 2020  Etienne Malaboeuf

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

 * 
 */

#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include "snptp.h"
#include "snptrp.h"

void PrintHelp()
{
    puts(
        "./app_demo PREV_NODE_LISTEN LOCAL_IP_TO_BIND NEXT_NODE_SEND PORT_LISTEN PORT_SEND [--origin]\n"
        "--origin tells the program to send the first token on the ring. You want only one peer to have this flag.\n"
        "Be careful to enter coherent IPs and ports!");
}

void* SntprpThread(void* ctx_snptrp)
{
    printf("Starting the Snptrp processing loop thread, TICKRATE %dµs\n", SNPTRP_SLEEP_TIME_USEC);
    while(true)
    {
        if(snptrp_ProtoProcessLoop(ctx_snptrp) < 0)
        {
            perror("SNPTRP thread stoped");
            break;
        }
        usleep(SNPTRP_SLEEP_TIME_USEC);
    }
    puts("Snptrp thread just closed");
    return NULL;
}

void* SntpThread(void* ctx_snptp)
{
    printf("Starting the Snptp processing loop thread, TICKRATE %dµs\n", SNPTP_SLEEP_TIME_USEC);
    while(true)
    {
        if(snptp_ProtoProcessLoop(ctx_snptp) < 0)
        {
            perror("SNPTP thread stoped");
            break;
        }
        usleep(SNPTP_SLEEP_TIME_USEC);
    }
    puts("Snptp thread just closed");
    return NULL;
}

char* ExtractAddr(char* str, size_t len, size_t* idx)
{
    static char addr[15 + 1];
    char*       addr_ptr = addr;

    enum ip_parsing_state
    {
        START,
        NUMBER
    };

    int                   dot_count    = 0;
    int                   number_count = 0;
    enum ip_parsing_state state        = START;

    for(size_t i = 0; i < len; ++i)
    {
        *idx = i;
        switch(state)
        {
            case START:
            {
                switch(str[i])
                {
                    case '0' ... '9':
                        number_count    = 1;
                        state           = NUMBER;
                        (*(addr_ptr++)) = str[i];
                        break;
                    default:
                        break;
                }
                break;
            }
            case NUMBER:
            {
                switch(str[i])
                {
                    case '0' ... '9':
                        if(++number_count > 3)
                        {
                            return NULL;
                        }

                        *(addr_ptr++) = str[i];
                        if(number_count == 3 && dot_count == 3)
                        {
                            ++(*idx);
                            *(addr_ptr++) = '\0';
                            return addr;
                        }
                        break;
                    case '.':
                        if(++dot_count > 3)
                        {
                            return NULL;
                        }
                        number_count  = 0;
                        *(addr_ptr++) = str[i];
                        break;

                    default:
                        if(number_count > 0 && dot_count == 3)
                        {
                            *(addr_ptr++) = '\0';
                            return addr;
                        }
                        return NULL;
                }
                break;
            }
            default:
                return NULL;
        }
    }

    return NULL;
}

int main(const int argc, char* argv[])
{
    bool original = false;

    char* my_addr = NULL;
    char* my_dest = NULL;
    char* my_prev = NULL;

    char* port_listen = NULL;
    char* port_send   = NULL;

    if(argc < 6)
    {
        PrintHelp();
        exit(EXIT_FAILURE);
    }

    if(argc == 7 && strcmp(argv[6], "--original") == 0)
    {
        original = true;
    }

    my_prev = argv[1];
    my_addr = argv[2];
    my_dest = argv[3];

    port_listen = argv[4];
    port_send   = argv[5];

    printf(
        "Définition d'une node de l'anneau avec l'ip %s,\n"
        "la destination %s et la machine précédente %s.\n",
        my_addr,
        my_dest,
        my_prev);

    snptrp_Context_t ctx_snptrp;
    snptp_Context_t  ctx_snptp;

    if(snptrp_InitCtx(&ctx_snptrp, my_addr, my_dest, my_prev, port_listen, port_send) < 0)
    {
        perror("Could not init the snptrp ctx");
        exit(EXIT_FAILURE);
    }

    if(snptp_InitCtx(&ctx_snptp, &ctx_snptrp) < 0)
    {
        perror("Could not init the snptrp ctx");
        exit(EXIT_FAILURE);
    }

    if(original)
    {
        snptrp_SetOriginal(&ctx_snptrp);
    }

    pthread_t snptrp;
    pthread_t snptp;

    if(pthread_create(&snptrp, NULL, SntprpThread, &ctx_snptrp) < 0)

    {
        exit(EXIT_FAILURE);
    }
    if(pthread_create(&snptp, NULL, SntpThread, &ctx_snptp) < 0)

    {
        exit(EXIT_FAILURE);
    }

    while(true)
    {
        char   buff[1 << 10];
        size_t bytes_read = 0;

        for(size_t i = 0; (i < sizeof(buff) - 1); ++i)
        {
            bytes_read = i + 1;
            buff[i]    = getchar();
            if(buff[i] == '\0' || buff[i] == '\n')
            {
                buff[i] = '\0';
                break;
            }
        }

        // TODO check syntax and send/recv.

        char*  addr_str;
        size_t idx;
        if(bytes_read >= 7 /* sizeof("c 1.1.1.1"  */)
        {
            if((addr_str = ExtractAddr(buff, bytes_read, &idx)) != NULL)
            {
                /* printf("%s end at %ld with '%c'\n", addr_str, idx, buff[idx]); */
            }
            else
            {
                printf("Bad addr\n");
                continue;
            }
        }
        else
        {
            if(buff[0] == 'e')
            {
                printf("Exiting, bbye\n");

                // TODO message threads and wait on join().

                exit(EXIT_SUCCESS);
            }
            printf("Again pls..\n");
            continue;
        }

        switch(buff[0])
        {
            case 'c':
            {
                printf("Trying to connect to %s\n", addr_str);
                if(snptp_Connect(&ctx_snptp, addr_str) < 0)
                {
                    perror("Failed to connect");
                    continue;
                }
                puts("Syn flag sent to lower layer queue");
            }
            break;

            case 'd':
            {
                printf("Trying to deconnect from %s\n", addr_str);
                if(snptp_Close(&ctx_snptp, addr_str) < 0)
                {
                    perror("Failed to close");
                    continue;
                }
                puts("Fin flag sent to lower layer queue");
            }
            break;

            case 's':
            {
                ++idx; /* s 1.1.1.1[space]data */
                size_t data_len = strlen(buff + idx);
                if(idx >= bytes_read || data_len == 0)
                {
                    printf("Again pls, no payload..\n");
                    continue;
                }

                printf("Sending %ld bytes to '%s': '%s'\n", data_len, addr_str, buff + idx);
                if(snptp_WriteTo(&ctx_snptp, (Payload8_t*)(buff + idx), data_len, addr_str) < 0)
                {
                    perror("Failed to send data");
                    continue;
                }
                puts("Data in queue sent to lower queue");
            }
            break;
            case 'r':
            {
                ssize_t snptp_bytes_read;

                printf("Trying to receive from %s\n", addr_str);
                if((snptp_bytes_read = snptp_ReadFrom(&ctx_snptp, (Payload8_t*)buff, sizeof(buff) - 1, addr_str)) < 0)
                {
                    perror("Failed to read data");
                    continue;
                }

                buff[snptp_bytes_read] = '\0';

                printf("Bytes read: '%s'\n", buff);
            }
            break;
            default:
                printf("Again pls..\n");
                continue;
        }
    }

    pthread_join(snptp, NULL);
    pthread_join(snptrp, NULL);
    return 0;
}
